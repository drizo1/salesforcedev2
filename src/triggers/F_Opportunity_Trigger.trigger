trigger F_Opportunity_Trigger on Opportunity (after delete, after insert, after undelete, after update, before delete, before insert, before update)  { 
  if (!System.isBatch()  || !System.isFuture()) {
    F_Trigger_Opportunity ct = new F_Trigger_Opportunity(trigger.isAfter, trigger.isBefore, trigger.isDelete, trigger.isInsert, trigger.isUnDelete, trigger.isUpdate, trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
    ct.run();
  }
}