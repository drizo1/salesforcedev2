trigger KS_Trigger_LLamadas on KS_Llamadas__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	if (!System.isBatch()  || !System.isFuture()) {
		KS_LLamadas_Trigger lt = new KS_LLamadas_Trigger(trigger.isAfter, trigger.isBefore, trigger.isDelete, trigger.isInsert, trigger.isUnDelete, trigger.isUpdate, trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
		lt.run();
	}
}