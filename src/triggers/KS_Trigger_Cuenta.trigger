trigger KS_Trigger_Cuenta on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update)  { 
	if (!System.isBatch()  || !System.isFuture()) {
		KS_Cuenta_Trigger ct = new KS_Cuenta_Trigger(trigger.isAfter, trigger.isBefore, trigger.isDelete, trigger.isInsert, trigger.isUnDelete, trigger.isUpdate, trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
		ct.run();
	}
}