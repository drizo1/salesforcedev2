public class KS_Account_AlarmOpportunity_Apex {

    @AuraEnabled
    public static void sendMail(String destino, String subject, String body, String parentID) {
        
        String pdf = getPDFlink(parentID);
        
        System.debug('-- JCAR -- sendEmail -- -- - -- -- -- - destino - ' + destino);
        System.debug('-- JCAR -- sendEmail -- -- - -- -- -- - body - ' + body);
        System.debug('-- JCAR -- sendEmail -- -- - -- -- -- - pdf - ' + pdf);
        
        if (pdf != null && pdf.trim() != '') {
            body += '\r\n\r\n' + pdf;
        } else { return; }
        
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        emailMessage.setSubject(subject);
        String [] toAddresses = new String[] { destino };
        emailMessage.setToAddresses(toAddresses);
        emailMessage.setPlainTextBody(body);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { emailMessage });
        
        System.debug('-- JCAR -- sendEmail -- -- - -- -- -- - DONE');
    }
    private static String getPDFlink(String parentID) {
        
        try { 
            KS_Alarm_and_Opportunity__c obj = getAlarmByID(parentID);
            return obj.KS_Link_to_PDF__c;
        } catch (Exception ex) {}
        try { 
            KS_Proposed_Opportunity__c obj = getOpProposedByID(parentID);
            return obj.KS_Link_to_PDF__c;
        } catch (Exception ex) {}
        return '';
    }
    @AuraEnabled
    public static String auraPDFlink(String parentID) { return getPDFlink(parentID); }
    
    public static KS_Alarm_and_Opportunity__c getAlarmByID(Id recordID) {
        return [SELECT ID, KS_Business_Unit__c, KS_Familia_estrat_gica__c, 
                KS_Value__c, KS_Alarm_measure__c, KS_Link_to_PDF__c, KS_Account__c
                FROM KS_Alarm_and_Opportunity__c 
                WHERE ID = :recordId];
    }
    public static KS_Proposed_Opportunity__c getOpProposedByID(Id recordID) {
        return [SELECT ID, KS_Business_Unit__c, KS_Familia_Estrategica__c, 
                KS_Value__c, KS_Unit_of_measure__c, KS_Link_to_PDF__c, KS_Cuenta__c 
                FROM KS_Proposed_Opportunity__c 
                WHERE ID = :recordId];
    }
    
    public static Map<Id,KS_Alarm_and_Opportunity__c> getOppsAlarmMap(Id recordId, String tipo) {
        if (tipo == 'Alarm') {
            return new Map<Id, KS_Alarm_and_Opportunity__c>(
                [SELECT ID, KS_Business_Unit__c, KS_Familia_estrat_gica__c, 
                 KS_Value__c, KS_Alarm_measure__c, KS_Link_to_PDF__c, KS_Account__c
                 FROM KS_Alarm_and_Opportunity__c 
                 WHERE KS_Account__c = :recordId /*AND KS_Tipo_de_entrada__c = :tipo*/]); //Opportunity OR Alarm
        }
        else if (tipo == 'Opportunity') {
            Map<Id, KS_Alarm_and_Opportunity__c> mapMe = new Map<Id, KS_Alarm_and_Opportunity__c>();
            for (KS_Proposed_Opportunity__c propp : 
                 [SELECT ID, KS_Business_Unit__c, KS_Familia_Estrategica__c, 
                  KS_Value__c, KS_Unit_of_measure__c, KS_Link_to_PDF__c, KS_Cuenta__c 
                  FROM KS_Proposed_Opportunity__c 
                  WHERE KS_Cuenta__c = :recordId]){
                      
                      KS_Alarm_and_Opportunity__c alopp = new KS_Alarm_and_Opportunity__c();
                      alopp.KS_Business_Unit__c = propp.KS_Business_Unit__c;
                      alopp.KS_Familia_estrat_gica__c = propp.KS_Familia_Estrategica__c;
                      alopp.KS_Value__c = propp.KS_Value__c;
                      alopp.KS_Alarm_measure__c = propp.KS_Unit_of_measure__c;
                      alopp.KS_Account__c = propp.KS_Cuenta__c;
                      alopp.KS_Link_to_PDF__c = propp.KS_Link_to_PDF__c;
                      mapMe.put(propp.ID, alopp);
                  }
            return mapMe;
        }
        return new Map<Id,KS_Alarm_and_Opportunity__c>();
    }
    
    @AuraEnabled
    public static List<KS_Alarm_and_Opportunity__c> getOppsAlarm(Id recordId, String tipo) {
        System.debug('-- JCAR -- getOppsAlarm -- -- - -- -- -- - recordId - ' + recordId);
        System.debug('-- JCAR -- getOppsAlarm -- -- - -- -- -- - tipo - ' + tipo);
        
        Map<Id,KS_Alarm_and_Opportunity__c> oppsAlarmMap = KS_Account_AlarmOpportunity_Apex.getOppsAlarmMap(recordId, tipo);
        System.debug('-- JCAR -- getOppsAlarm -- -- - -- -- -- - oppsAlarmMap - ' + oppsAlarmMap);
        return oppsAlarmMap.values();
    }
    
    @AuraEnabled
    public static List<KS_Alarm_and_Opportunity__c> getOppsAlarmWithConDoc(Id recordId, String tipo, Boolean withAttach) {
        
        System.debug('-- JCAR -- getOppsAlarm -- -- - -- -- -- - recordId - ' + recordId);
        System.debug('-- JCAR -- getOppsAlarm -- -- - -- -- -- - tipo - ' + tipo);
        
        Map<Id,KS_Alarm_and_Opportunity__c> oppsAlarmMap = KS_Account_AlarmOpportunity_Apex.getOppsAlarmMap(recordId, tipo);
        System.debug('-- JCAR -- getOppsAlarm -- -- - -- -- -- - oppsAlarmMap - ' + oppsAlarmMap);
        if (!withAttach) { return oppsAlarmMap.values(); }
        
        Map<ID,ContentVersion> oppAlarmDocuments = KS_ContentDocument_Utils.getMapContentVersionByParentIDSet(oppsAlarmMap.keySet());
        System.debug('-- JCAR -- getOppsAlarm -- -- - -- -- -- - oppAlarmDocuments - ' + oppAlarmDocuments);
        if (!oppAlarmDocuments.isEmpty()) {
            for (ID codDocID : oppAlarmDocuments.keySet()) {
                ContentVersion cv = oppAlarmDocuments.get(codDocID);
                if (oppsAlarmMap.containsKey(cv.KS_ParentID__c)) {
                    KS_Alarm_and_Opportunity__c alOpp = oppsAlarmMap.get(cv.KS_ParentID__c);
                    alOpp.KS_Link_to_PDF__c = codDocID;
                    oppsAlarmMap.put(cv.KS_ParentID__c, alOpp);
                }
            }
        }
        
        return oppsAlarmMap.values();
    }
}