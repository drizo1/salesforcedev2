public class CtrlVisitReportPDF {

    public KS_Visit_Report__c vr {get;set;}
    public Account a{ get; set; }
    public String seleccionado{get;set;}
    public Id idresp{get;set;}
    public String nombreVR{get;set;}

    public Boolean newcli1 {get;set;}
    public Boolean evventa1 {get;set;}
    public Boolean marketing1 {get;set;}
    public Boolean segop1 {get;set;}
    public Boolean puertaab1 {get;set;}
    public Boolean problogis1 {get;set;}
    public Boolean porbpostv1 {get;set;}
    public Boolean prescondic1 {get;set;}
    public Boolean incidecob1 {get;set;}
    public Boolean visitacc1 {get;set;}

    public Boolean bomba1 {get;set;}
    public Boolean cubierta1 {get;set;}
    public Boolean electro1 {get;set;}
    public Boolean condu1 {get;set;}
    public Boolean filtro1 {get;set;}
    public Boolean bombca1 {get;set;}
    public Boolean iop1 {get;set;}
    public Boolean led1 {get;set;}
    public Boolean quimic1 {get;set;}
    public Boolean riego1 {get;set;}
    public Boolean robot1 {get;set;}
    public Boolean otro1 {get;set;}

    public String visitacoor {get;set;}
    public String objalc {get;set;}
    public String porque {get;set;}

    public Boolean evventa2 {get;set;}
    public Boolean marketing2 {get;set;}
    public Boolean segop2 {get;set;}
    public Boolean puertaab2 {get;set;}
    public Boolean problogis2 {get;set;}
    public Boolean porbpostv2 {get;set;}
    public Boolean prescondic2 {get;set;}
    public Boolean incidecob2 {get;set;}
    public Boolean visitacc2 {get;set;}

    public Boolean bomba2 {get;set;}
    public Boolean cubierta2 {get;set;}
    public Boolean electro2 {get;set;}
    public Boolean condu2 {get;set;}
    public Boolean filtro2 {get;set;}
    public Boolean bombca2 {get;set;}
    public Boolean iop2 {get;set;}
    public Boolean led2 {get;set;}
    public Boolean quimic2 {get;set;}
    public Boolean riego2 {get;set;}
    public Boolean robot2 {get;set;}
    public Boolean otro2 {get;set;}

    public DateTime proximavis {get;set;}

    public CtrlVisitReportPDF (ApexPages.StandardController stdController)
    {
            String look_id = ApexPages.currentPage().getParameters().get('id');
            vr = getOne2OneReport(new KS_Visit_Report__c(id = look_id));
            seleccionado = vr.KS_Account__r.name == null ? ApexPages.currentPage().getParameters().get('seleccionado') : String.valueOf(vr.KS_Account__r.name);
            idresp = vr.KS_Account__c;

            newcli1 = vr.F_Nuevo_Cliente__c;
            evventa1 = vr.F_Evoluci_n_de_ventas__c;
            marketing1 = vr.F_Marketing__c;
            segop1 = vr.F_Seguimiento_Oportunidad__c;
            puertaab1 = vr.F_Puertas_Abiertas__c;
            problogis1 = vr.F_Problema_de_Logistica__c;
            porbpostv1 = vr.F_Problema_de_post_venta__c;
            prescondic1 = vr.F_Presentaci_n_Condiciones_Comerciales__c;
            incidecob1 = vr.F_Incidencias_cobros__c;
            visitacc1 = vr.F_Visita_Cash_Carry__c;

            bomba1 = vr.F_Bombas__c;
            cubierta1 = vr.F_Cubiertas__c;
            electro1 = vr.F_Electrolisis__c;
            condu1 = vr.F_Conducci_n_de_Fluidos__c;
            filtro1 = vr.F_Filtros__c;
            bombca1 = vr.F_Bombas_de_Calor__c;
            iop1 = vr.F_IoP_Conectividad__c;
            led1 = vr.F_LED__c;
            quimic1 = vr.F_Qu_mico__c;
            riego1 = vr.F_Riego__c;
            robot1 = vr.F_Robots__c;
            otro1 = vr.F_Otros__c;

            visitacoor = vr.Visita_Coord_Manager__c;
            objalc = vr.F_He_alcanzado_mis_objetivos__c;
            porque = vr.F_Porque__c;

            evventa2 = vr.F_Evoluci_n_de_ventas2__c;
            marketing2 = vr.F_Marketing2__c;
            segop2 = vr.F_Seguimiento_Oportunidades2__c;
            puertaab2 = vr.F_Puertas_Abiertas2__c;
            problogis2 = vr.F_Problemas_de_Log_stica2__c;
            porbpostv2 = vr.F_Problema_de_post_venta2__c;
            prescondic2 = vr.F_Presentacion_de_condiciones2__c;
            incidecob2 = vr.F_Incidencias_cobros2__c;
            visitacc2 = vr.F_Visita_Cash_Carry2__c;

            bomba2 = vr.F_Bombas2__c;
            cubierta2 = vr.F_Cubiertas2__c;
            electro2 = vr.F_Electrolisis2__c;
            condu2 = vr.F_Conducci_n_de_Fluidos2__c;
            filtro2 = vr.F_Filtros2__c;
            bombca2 = vr.F_Bombas_de_Calor2__c;
            iop2 = vr.F_IoP_Conectividad2__c;
            led2 = vr.F_LED2__c;
            quimic2 = vr.F_Qu_mico2__c;
            riego2 = vr.F_Riego2__c;
            robot2 = vr.F_Robots2__c;
            otro2 = vr.F_Otros2__c;

            proximavis = vr.Fecha_pr_xima_visita__c;
            nombreVR = vr.Name;


            String yourFileName = '';
            yourFileName = 'Visit_Report_'+nombreVR+'_'+seleccionado+'_'+Date.today().format()+'.pdf';
            Apexpages.currentPage().getHeaders().put( 'content-disposition', 'filename=' + yourFileName  ); 
    }

    public static KS_Visit_Report__c getOne2OneReport(KS_Visit_Report__c visRep) {
        if (visRep == null) {
            return new KS_Visit_Report__c();
        }
        if (visRep.ID == null) {
            return visRep;
        }

        try {
            KS_Visit_Report__c vr = [SELECT id, KS_Account__r.Name, Name, KS_Account__c,F_Nuevo_Cliente__c,F_Evoluci_n_de_ventas__c,
                                     F_Marketing__c, F_Seguimiento_Oportunidad__c, F_Puertas_Abiertas__c, F_Problema_de_Logistica__c, F_Problema_de_post_venta__c,
                                     F_Presentaci_n_Condiciones_Comerciales__c, F_Incidencias_cobros__c, F_Visita_Cash_Carry__c,
                                     F_Bombas__c, F_Cubiertas__c, F_Electrolisis__c, F_Conducci_n_de_Fluidos__c, F_Filtros__c, F_Bombas_de_Calor__c,
                                     F_IoP_Conectividad__c, F_LED__c, F_Qu_mico__c, F_Riego__c, F_Robots__c, F_Otros__c, Visita_Coord_Manager__c,
                                     F_He_alcanzado_mis_objetivos__c, F_Porque__c, F_Fecha_Pr_xima_Visita__c, F_Evoluci_n_de_ventas2__c, F_Marketing2__c,
                                     F_Seguimiento_Oportunidades2__c, F_Puertas_Abiertas2__c, F_Problemas_de_Log_stica2__c, F_Problema_de_post_venta2__c,
                                     F_Presentacion_de_condiciones2__c, F_Incidencias_cobros2__c, F_Visita_Cash_Carry2__c, F_Bombas2__c, F_Cubiertas2__c,
                                     F_Electrolisis2__c, F_Conducci_n_de_Fluidos2__c, F_Filtros2__c, F_Bombas_de_Calor2__c, F_IoP_Conectividad2__c,
                                     F_LED2__c, F_Qu_mico2__c, F_Riego2__c, F_Robots2__c, F_Otros2__c,Fecha_pr_xima_visita__c, ContadorV__c
                                     FROM KS_Visit_Report__c  where ID = :visRep.ID];
            return vr;
        } catch (Exception ex) {
            return visRep;
        }
    }    
}