public class CtrlAdm_report121 {
    public List<SelectOption> users { get; set; }
    public X1_to_1_Report__c vr {
        get;set;
    }
    public Account a{ get; set; }
	public String seleccionado{get;set;}
	public Id idresp{get;set;}
	public String events{get;set;}
	public String inputValue{get;set;}

    public Decimal ratingEstrategia {get;set;}
    public Decimal ratingSOW {get;set;}
    public Decimal ratingMarketing {get;set;}
    public Decimal ratingAgenda {get;set;}
    public Decimal ratingCalidad {get;set;}
    public Decimal ratingAcompanamiento {get;set;}
    public Decimal ratingPreparacion {get;set;}
    public Decimal ratingSeguimiento {get;set;}
    public Decimal ratingPalancas {get;set;}
    public Decimal ratingeRatio {get;set;}

    public String comentarioEstrategia {get;set;}
    public String comentarioSOW {get;set;}
    public String comentarioMarketing {get;set;}
    public String comentarioAgenda {get;set;}
    public String comentarioCalidad {get;set;}
    public String comentarioAcompanamiento {get;set;}
    public String comentarioPreparacion {get;set;}
    public String comentarioSeguimiento {get;set;}
    public String comentarioPalancas {get;set;}
    public String comentarioRatio {get;set;}

    public String accionEstrategia {get;set;}
    public String accionSOW {get;set;}
    public String accionMarketing {get;set;}
    public String accionAgenda {get;set;}
    public String accionCalidad {get;set;}
    public String accionAcompanamiento {get;set;}
    public String accionPreparacion {get;set;}
    public String accionSeguimiento {get;set;}
    public String accionPalancas {get;set;}
    public String accionRatio {get;set;}

    public String comentarioGlobal {get;set;}
    public Decimal ratioGlobal {get;set;}

	Date myDate = system.today();
    public Boolean initialised{get; set;}



    public CtrlAdm_report121(ApexPages.StandardController stdController)
    {
    		String look_id = ApexPages.currentPage().getParameters().get('id');
    		vr = getOne2OneReport(new X1_to_1_Report__c(id = look_id));
    	    seleccionado = vr.User__r.name == null ? ApexPages.currentPage().getParameters().get('seleccionado') : String.valueOf(vr.User__r.name);
    	    idresp = vr.User__c;

            ratingEstrategia = decimal.valueOf(vr.Desarrollo_de_la_estrategia__c);
            comentarioEstrategia = vr.Coment_Estrategia__c;
            accionEstrategia = vr.Acci_n_estrategia__c;

            ratingSOW = decimal.valueOf(vr.Incrementar_el_SOW__c);
            comentarioSOW = vr.Coment_SOW__c;
            accionSOW = vr.Acci_n_SOW__c;

            ratingMarketing = decimal.valueOf(vr.Utilizaci_n_de_campa_a_de_Marketing__c);
            comentarioMarketing = vr.Coment_Marketing__c;
            accionMarketing = vr.Acci_n_Marketing__c;

            ratingAgenda = decimal.valueOf(vr.Agenda_de_visitas_en_Salesforce__c);
            comentarioAgenda = vr.Coment_visit_SF__c;
            accionAgenda = vr.Acci_n_Agenda__c;

            ratingCalidad = decimal.valueOf(vr.Calidad_de_Visit_Report__c);
            comentarioCalidad = vr.Coment_Visit_Report__c;
            accionCalidad = vr.Acci_n_Calidad__c;

            ratingAcompanamiento = decimal.valueOf(vr.Acompa_amiento_al_equipo__c);
            comentarioAcompanamiento = vr.Coment_acompa_amiento__c;
            accionAcompanamiento = vr.Acci_n_acompa_amiento__c;

            ratingPreparacion = decimal.valueOf(vr.Preparaci_n_de_la_entrevista__c);
            comentarioPreparacion = vr.Coment_entrevista__c;
            accionPreparacion = vr.Acci_n_entrevista__c;

            ratingSeguimiento = decimal.valueOf(vr.Seguimiento_Ventas_vs_Potencial__c);
            comentarioSeguimiento = vr.Coment_Venta_Pot__c;
            accionSeguimiento = vr.Acci_n_Seguimiento__c;

            ratingPalancas = decimal.valueOf(vr.Evoluci_n_Palancas_de_Crecimientos__c);
            comentarioPalancas = vr.Coment_Palancas__c;
            accionPalancas = vr.Acci_n_palanca__c;

            ratingeRatio = decimal.valueOf(vr.Ratio_Check_in_vs_Visit_Report__c);
            comentarioRatio = vr.Coment_Check_in__c;
            accionRatio = vr.Acci_n_ratio__c;

            ratioGlobal = vr.Ratingtotal__c;
            comentarioGlobal = vr.Comentarios_121__c;

            String yourFileName = '';
            yourFileName = 'ONE2ONE_Report_'+seleccionado+'_'+Date.today().format()+'.pdf';
            Apexpages.currentPage().getHeaders().put( 'content-disposition', 'filename=' + yourFileName  );    
    }	


    public static X1_to_1_Report__c getOne2OneReport(X1_to_1_Report__c visRep) {
        if (visRep == null) {
            return new X1_to_1_Report__c();
        }
        if (visRep.ID == null) {
            return visRep;
        }

        try {
            X1_to_1_Report__c vr = [SELECT id, User__r.name, User__c, Desarrollo_de_la_estrategia__c, Coment_Estrategia__c, Acci_n_estrategia__c,
                                    Incrementar_el_SOW__c, Coment_SOW__c, Acci_n_SOW__c,
                                    Utilizaci_n_de_campa_a_de_Marketing__c, Coment_Marketing__c, Acci_n_Marketing__c,
                                    Agenda_de_visitas_en_Salesforce__c, Coment_visit_SF__c, Acci_n_Agenda__c,
                                    Calidad_de_Visit_Report__c, Coment_Visit_Report__c, Acci_n_Calidad__c,
                                    Acompa_amiento_al_equipo__c, Coment_acompa_amiento__c, Acci_n_acompa_amiento__c,
                                    Preparaci_n_de_la_entrevista__c, Coment_entrevista__c, Acci_n_entrevista__c,
                                    Seguimiento_Ventas_vs_Potencial__c, Coment_Venta_Pot__c, Acci_n_Seguimiento__c,
                                    Evoluci_n_Palancas_de_Crecimientos__c, Coment_Palancas__c, Acci_n_palanca__c,
                                    Ratio_Check_in_vs_Visit_Report__c, Coment_Check_in__c, Acci_n_ratio__c,
                                    Ratingtotal__c, Comentarios_121__c
				    FROM X1_to_1_Report__c  where ID = :visRep.ID];
            return vr;
        } catch (Exception ex) {
            System.debug('PETE ------------' + ex);
            return visRep;
        }
    }


    
}