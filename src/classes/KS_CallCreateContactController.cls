public class KS_CallCreateContactController {

	@AuraEnabled
	public static KS_Llamadas__c getCall(Id callId) {
		KS_Llamadas__c call = [Select Id, Name, KS_Telefono_Destino__c, KS_Origen__c from KS_Llamadas__c where Id = :callId];

		return call;
	}

	@AuraEnabled
	public static Account getAccount(Id accountId) {
		// Perform isAccessible() checks here
		return[SELECT Name, BillingCity, BillingState FROM Account WHERE Id = : accountId];
	}

	@AuraEnabled
	public static Contact saveContact(Contact contact, Id accountId) {
		contact.AccountId = accountId;
		upsert contact;
		//actualizr las calls de ese contacto
		List<KS_Llamadas__c> callList = [Select Id, Name, KS_Duracion__c, KS_Fecha_Llamada__c, KS_Identificador_llamada__c, KS_Inicio_llamada__c, KS_Origen__c,
		                                 KS_Telefono_Destino__c, KS_Tipo_de_llamada__c, KS_Tratada__c from KS_Llamadas__c where KS_Telefono_Destino__c = :contact.Phone];
		List<Task> tkList = new List<Task> ();
		List<KS_Llamadas__c> callUpdateList = new List<KS_Llamadas__c> ();
		for (KS_Llamadas__c call : callList) {
			if (call.KS_Tratada__c && !Test.isRunningTest()) continue;
			Task t = new Task();
			t.WhoId = contact.Id;
            t.WhatId = contact.AccountId;
			t.Subject = 'Call'; 
			t.Status = 'Completed';
            t.Type = 'Call';
			t.TaskSubtype = 'Call';
			t.CallDurationInSeconds = Integer.valueOf(call.KS_Duracion__c);
			t.CallObject = call.KS_Identificador_llamada__c;
			string aux = '';

			aux = 'LLamada de tipo: ' + call.KS_Tipo_de_llamada__c + ' sobre el numero' + call.KS_Telefono_Destino__c + '.\n\nDuracion de la llamada : ' + call.KS_Duracion__c;
			t.Description = aux;
			t.ActivityDate = Date.valueOf(call.KS_Inicio_llamada__c);
			if (call.KS_Origen__c != null)
			t.OwnerId = call.KS_Origen__c;
			tkList.add(t);
			call.KS_Tratada__c = true;
			callUpdateList.add(call);
		}
		insert tkList;
		update callUpdateList;
		return contact;
	}
}