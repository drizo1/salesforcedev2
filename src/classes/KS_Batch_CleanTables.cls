global class KS_Batch_CleanTables implements Database.Batchable<sObject>{
    
    global final String query;
    
    global KS_Batch_CleanTables(String q){
        
        query=q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        delete scope;
    } 
    
    global void finish(Database.BatchableContext BC){
        System.debug('Axel-------------------------- Borrado correctamente');
        /*AsyncApexJob a = [SELECT Id FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        system.abortJob(a.id);*/
    }
}