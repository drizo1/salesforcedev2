public with sharing class KS_Opportunity_Trigger extends KS_TriggerController {
    
    public KS_Opportunity_Trigger(Boolean isAfter, Boolean isBefore, Boolean isDelete, Boolean isInsert, Boolean isUndelete, Boolean isUpdate, List<sObject> lstNewItems, Map<Id, sObject> mapNewItems, List<sObject> lstOldItems, Map<Id, sObject> mapOldItems)
    {
        super(isAfter, isBefore, isDelete, isInsert, isUndelete, isUpdate, lstNewItems, mapNewItems, lstOldItems, mapOldItems);
    }
    
    protected override void runBeforeInsert() {
        ponerCodautomatico();
    } 

    private void ponerCodautomatico() {
        Decimal[] names = new Decimal[] {};
        Decimal valuenew = 0;
     
     for (Opportunity opp : [SELECT new_code__c FROM Opportunity WHERE RecordTypeId = '0121q0000008W3IAAU' and new_code__c != null ORDER BY new_code__c DESC LIMIT 1]) {
          names.add(opp.new_code__c);
        }
        
     valuenew = names[0];
     System.debug(names);

    }
    
}