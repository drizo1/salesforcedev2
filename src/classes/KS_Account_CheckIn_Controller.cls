public with sharing class KS_Account_CheckIn_Controller {

    private static Double distMaxi = 0.1; //1km
    private static String distUnit = 'km';
    
    @AuraEnabled
    public static Account searchAccount (String accID) {

        System.debug('-- JCAR -- searchAccount -- -- - -- -- -- - accID - ' + accID);
        
        List<Account> accs =  [SELECT ID, name, BillingLatitude, BillingLongitude,
                               KS_Location__Latitude__s, KS_Location__Longitude__s,
                               KS_CheckIn__c FROM Account
                               WHERE ID = :accID LIMIT 1];
        System.debug('-- JCAR -- searchAccount -- -- - -- -- -- - ' + accs.size() + ' - CUENTAS - ' + accs);
        if (accs.isEmpty()) { return null; }
        Account acc = accs.get(0);
        acc.KS_CheckIn__c = !KS_Account_CheckIn_Controller.getChecks(accID).isEmpty();
        System.debug('-- JCAR -- searchAccount -- -- - -- -- -- - acc.KS_CheckIn__c ? ' + acc.KS_CheckIn__c);
        
        return acc;
    }
    
    @AuraEnabled
    public static List<String> checkInAccount (String accID, Decimal userLat, Decimal userLon) {

        System.debug('-- JCAR -- checkInAccount -- -- - -- -- -- - userLat - ' + userLat);
        System.debug('-- JCAR -- checkInAccount -- -- - -- -- -- - userLon - ' + userLon);
        if (userLat == null) { return KS_Account_CheckIn_Controller.sendResponse('ERROR_GEO_USER'); }
        if (userLon == null) { return KS_Account_CheckIn_Controller.sendResponse('ERROR_GEO_USER'); }
        
        Account acc = KS_Account_CheckIn_Controller.searchAccount(accID);
        if (acc == null) { return KS_Account_CheckIn_Controller.sendResponse('ERROR'); }
        if (acc.BillingLatitude == null && acc.KS_Location__Latitude__s == null) 
        { return KS_Account_CheckIn_Controller.sendResponse('ERROR_GEO_ACC'); }
        if (acc.BillingLongitude == null && acc.KS_Location__Longitude__s == null) 
        { return KS_Account_CheckIn_Controller.sendResponse('ERROR_GEO_ACC'); }

        if(KS_Account_CheckIn_Controller.getIsInReach(
            System.Location.newInstance(userLat, userLon),
            System.Location.newInstance(acc.KS_Location__Latitude__s != null ? acc.KS_Location__Latitude__s : acc.BillingLatitude, 
                                        acc.KS_Location__Longitude__s != null ? acc.KS_Location__Longitude__s : acc.BillingLongitude)
        )) {
            KS_CheckIn__c chk = new KS_CheckIn__c(KS_Account__c=accID,KS_DateTime_Ini__c=DateTime.now());
            insert chk;
            return KS_Account_CheckIn_Controller.sendResponse('VALID',chk.ID);
        } else {
            return KS_Account_CheckIn_Controller.sendResponse('INVALID');
        }
    }
    private static Boolean getIsInReach(System.Location loc1, System.Location loc2) {
        System.debug('-- JCAR -- getIsInReach -- -- - -- -- -- - distance? ' + 
                     Integer.valueOf(loc1.getDistance(loc2, KS_Account_CheckIn_Controller.distUnit)) + ' ' +  KS_Account_CheckIn_Controller.distUnit
                     + ' <= ' + Integer.valueOf(KS_Account_CheckIn_Controller.maxRadius()) + ' ' +  KS_Account_CheckIn_Controller.distUnit);
        
        return loc1.getDistance(loc2, KS_Account_CheckIn_Controller.distUnit) <= KS_Account_CheckIn_Controller.maxRadius();
    }
    
    private static Decimal maxRadius() {
        KS_CONF__c conf = [SELECT NAME, KS_Value_Numeric__c FROM KS_CONF__c where NAME = 'CheckIn_Distance'];
        return conf != null ? (conf.KS_Value_Numeric__c/1000) : 10; // convierto metros en km
    }
    
    @AuraEnabled
    public static List<String> checkOutAccount (String accID) {
        
        System.debug('-- JCAR -- checkOutAccount -- -- - -- -- -- - accID - ' + accID);
        if (accID == null) { return KS_Account_CheckIn_Controller.sendResponse('CHECKOUT_KO'); }
        List<KS_CheckIn__c> chks = KS_Account_CheckIn_Controller.getChecks(accID);
        if (chks.isEmpty()) { return KS_Account_CheckIn_Controller.sendResponse('CHECKOUT_KO'); } 
        System.debug('-- JCAR -- checkOutAccount -- -- - -- -- -- - KS_CheckIn__c - ' + chks.get(0));
        
        KS_CheckIn__c chk = chks.get(0); chk.KS_DateTime_Fin__c = DateTime.now();
        System.debug('-- JCAR -- checkOutAccount -- -- - -- -- -- - KS_DateTime_Fin__c - ' + chk.KS_DateTime_Fin__c);
        update chk;

        return KS_Account_CheckIn_Controller.sendResponse('CHECKOUT_OK',chk.ID);
    }
    public static List<KS_CheckIn__c> getChecks(String accID) {
        if (accID == null) { return new List<KS_CheckIn__c>(); }
        
        DateTime dateToday = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        DateTime dateYeday = DateTime.newInstance(Date.today().addDays(1), Time.newInstance(0, 0, 0, 0));
        
        return [SELECT ID, KS_DateTime_Ini__c, KS_DateTime_Fin__c FROM KS_CheckIn__c 
                WHERE KS_Account__c = :accID AND KS_DateTime_Fin__c = null
                AND KS_DateTime_Ini__c >= :dateToday AND KS_DateTime_Ini__c < :dateYeday
                ORDER BY CreatedDate DESC LIMIT 1];
    }
    
    public static List<String> sendResponse(String msg) { return KS_Account_CheckIn_Controller.sendResponse(msg,null); }
    public static List<String> sendResponse(String msg, String value) {
        List<String> response = new List<String>();
        if (msg != null) { response.add(msg); if (value != null) { response.add(value); } }
        System.debug('-- JCAR -- sendResponse -- -- - -- -- -- - response - ' + response);        
        return response;
    }
}