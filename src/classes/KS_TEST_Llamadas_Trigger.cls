@isTest
public class KS_TEST_Llamadas_Trigger {
    public static testMethod void KS_LLamadas_Trigger() {  
        
        String telf = '62626262';
        
        Contact con = new Contact();
        con.Firstname = 'Pepito'; con.Lastname = 'Flores'; con.Phone = telf;
        insert con;

        KS_Llamadas__c l = new KS_Llamadas__c();
        l.KS_Duracion__c = 10;
        l.KS_Fecha_Llamada__c = Date.today();
        l.KS_Origen__c = UserInfo.getUserId();
        l.KS_Telefono_Destino__c = telf;     
        
        insert l;
        
        delete con;
        Lead led = new Lead();
        led.Firstname = 'Pepito'; led.Lastname = 'Flores'; led.Phone = telf;
        led.KS_phone_prefix__c = '32'; led.Company = 'Empresa SA';
        insert led;
        l.KS_Tratada__c = false;
        l.KS_Telefono_Destino__c = '00' + telf;
        update l;
        delete led;
        
        delete l;
        
    }
}