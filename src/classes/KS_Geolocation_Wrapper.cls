public class KS_Geolocation_Wrapper {

    private static String myselfKey = 'ME';
    private static Double distMaxi = 1; //1km
    private static String distUnit = 'km';
    
    public Map<String,KS_Geolocation_Marker> markerMap {get;set;} // key = Account.ID || if isME? key = myselfKey
    public Boolean getHaveData() { return markerMap != null && !markerMap.isEmpty(); }
    public Set<String> getMarkerIndex() { return (!getHaveData() ? new Set<String>() : markerMap.keySet());  }
    
    public KS_Geolocation_Wrapper() { markerMap = new Map<String,KS_Geolocation_Marker>(); }
    public void addMarker(Account acc, Boolean isMe) {
        markerMap = markerMap != null ? markerMap : new Map<String,KS_Geolocation_Marker>();
        KS_Geolocation_Marker mrk = new KS_Geolocation_Marker(acc, isMe);
        markerMap.put(isMe?myselfKey:acc.ID, mrk);
    }
    public void addMarker(Account acc) { addMarker(acc, false); }

    @AuraEnabled
    public KS_Geolocation_Marker getMyself() {
        if (!getHaveData()) {return null;}
        if (!markerMap.containsKey(myselfKey)) {return null;}
        System.debug('-- JCAR -- ------------------ getMyself? ' + markerMap.get(myselfKey));
        return markerMap.get(myselfKey);
    }
    
    public class KS_Geolocation_Marker {
        
        public Boolean isMe {get;set;}

        public Account acc {get;set;}
        @AuraEnabled
        public String getName() {
            if (acc == null) {return '';} if (acc.Name == null) {return '';}
            return acc.Name;
        }

        @AuraEnabled
        public Double getLat() {
            System.debug('-- JCAR -- ------------------ getLat? ' + acc.KS_Location__latitude__s);
            if (acc.KS_Location__latitude__s==null) {return 0;}
            return acc.KS_Location__latitude__s;
        }
        @AuraEnabled
        public Double getLon() {
            System.debug('-- JCAR -- ------------------ getLon? ' + acc.KS_Location__longitude__s);
            if (acc.KS_Location__longitude__s==null) {return 0;}
            return acc.KS_Location__longitude__s;
        }
        
        public Location getLocation() {
            return System.Location.newInstance(getLat(), getLon());
        }
        public Double getDistance(Location otherLoc) {
            System.debug('-- JCAR -- ------------------ getDistance?'+otherLoc);
            System.debug('-- JCAR -- ------------------ getLocation?'+getLocation());
            return getLocation().getDistance(otherLoc, KS_Geolocation_Wrapper.distUnit);
        }
        public Boolean getValidDistance(Location otherLoc) {
            return getDistance(otherLoc) <= KS_Geolocation_Wrapper.distMaxi;
        }
        
        public KS_Geolocation_Marker(Account acc, Boolean isMe) {
            this.acc = acc;
            this.isMe = isMe;
        }
    }
}