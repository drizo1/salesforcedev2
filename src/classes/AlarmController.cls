public class AlarmController {
     @AuraEnabled
    // public static String getAccountId(Id AccountId) 
    //public static List<KS_Alarm_and_Opportunity__c> getAlarms(Id AccountId) {
    //return [SELECT name,KS_Business_Unit__c,KS_Alarm_measure__c,KS_Familia_estrat_gica__c,KS_Value__c
    //FROM KS_Alarm_and_Opportunity__c WHERE KS_Tipo_de_entrada__c='Alarm' AND KS_Account__c=:AccountId ORDER BY KS_Familia_estrat_gica__c ASC];
      
    public static List<Account> getAlarms() {
    return [SELECT Id, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone
    FROM Account ORDER BY createdDate ASC];
  
}

}