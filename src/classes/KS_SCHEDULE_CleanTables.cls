global with sharing class KS_SCHEDULE_CleanTables implements Schedulable {
    
    global String nome;
    global void execute(SchedulableContext sc) { launchBatchCleanTables(); }
    global KS_SCHEDULE_CleanTables(String nombreEscogido){
        nome = nombreEscogido;
         System.debug('Axel-------------------------- ' + nombreEscogido);
    }   	
    global void launchBatchCleanTables() {
        
        string s = 'SELECT id FROM '+ nome;
        KS_Batch_CleanTables bachdel = new KS_Batch_CleanTables(s);
        Database.executeBatch(bachdel, 7000);
    }

    
    
}