public class KS_VisitReport_Controller {


    public KS_Visit_Report__c vr {
        get;set;
    }
    public String createdByName{
        get;set;
    }
    public String vrcreatedDate{
        get;set;
    }
    public String vrdateOfVisit{
        get;set;
    }
    public String vrId{
        get;set;
    }
    public String vrName{
        get;set;
    }
    public String accountName{
        get;set;
    }
    public String accountExteralId{
        get;set;
    }
    public String accountYTD{
        get;set;
    }
    public String accountVentasYTD{
        get;set;
    }
    public String vrComments{
        get;set;
    }
    public String leadName {
        get;set;
    }
    public String KS_ABS_Pipe_and_Fittings{
        get;set;
    }
    public String KS_Chemicals{
        get;set;
    }
    public String KS_Domestic_Pumps{
        get;set;
    }
    public String KS_Dosing_Equipment{
        get;set;
    }
    public String KS_Enclosures{
        get;set;
    }
    public String KS_Environmental_Control{
        get;set;
    }
    public String KS_Gas_and_Oil_Heaters{
        get;set;
    }
    public String KS_Heat_Pumps{
        get;set;
    }
    public String KS_Portable_Spas{
        get;set;
    }
    public String KS_Safety_Covers{
        get;set;
    }
    public String KS_Slatted_Covers{
        get;set;
    }
    public String KS_Connectivity{
        get;set;
    }
    public String KS_SWC{
        get;set;
    }
    public String KS_Automatic_Cleaners{
        get;set;
    }
    public String KS_Domestic_Filters{
        get;set;
    }
    public String KS_Commercial_Pumps{
        get;set;
    }
    public String KS_Commercial_Filters{
        get;set;
    }
     public String KS_Liners{
        get;set;
    }
    public String KS_On_site_Lining{
        get;set;
    }
    public String contactName
    {
        get;set;
    }


    //private final KS_Visit_Report__c add;

    public KS_VisitReport_Controller(ApexPages.StandardController controller) {

        System.debug('-- JCAR -- KS_VisitReport_Controller -- -- -- StandardController ' + controller);

        /*
         * try { add = (KS_Visit_Report__c)controller.getRecord(); }
         * catch (Exception ex) { System.debug('-- JCAR -- KS_VisitReport_Controller -- -- -- KS_Visit_Report__c ¡¡Unable to retrieve object!! '); }
         */

        String look_id = ApexPages.currentPage().getParameters().get('id');






        //System.debug('-- JCAR -- KS_VisitReport_Controller -- -- -- getRecord() ' + add);
        System.debug('-- JCAR -- KS_VisitReport_Controller -- -- -- get(id) ' + look_id);
        System.debug('-- JCAR -- KS_VisitReport_Controller -- -- -- get(contact name)' + contactName);


        vr = getVisitReportByID(new KS_Visit_Report__c(id = look_id));
        vrId = ApexPages.currentPage().getParameters().get('id');
        contactName = vr.KS_Contact__r.Name ==null ? ApexPages.currentPage().getParameters().get('contactname') : vr.KS_Contact__r.Name;
        createdByName = vr.CreatedBy.Name ==null ? ApexPages.currentPage().getParameters().get('createdby'): vr.CreatedBy.Name;
        vrcreatedDate = vr.CreatedDate == null ? ApexPages.currentPage().getParameters().get('createddate') : String.valueOf(vr.CreatedDate);
        vrdateOfVisit = vr.KS_Date_of_visit__c == null ? ApexPages.currentPage().getParameters().get('dateofvisit') : String.valueOf(vr.KS_Date_of_visit__c);
        vrName = vr.Name == null ? ApexPages.currentPage().getParameters().get('name') : vr.Name;
        vrComments = vr.KS_Comments__c == null ? ApexPages.currentPage().getParameters().get('comments') : vr.KS_Comments__c;
        /*vrComments = vrComments.substring(3);
        vrComments = vrComments.substring(0,vrComments.length()-4);
        vr.KS_Comments__c = vrComments.stripHtmlTags();*/
        //vrComments = vrComments.stripHtmlTags();
        vr.KS_Comments__c = vrComments;
        leadName = vr.Lead__r.Name == null ? ApexPages.currentPage().getParameters().get('leadname') : vr.Lead__r.Name;
        accountName = vr.KS_Account__r.Name == null ? ApexPages.currentPage().getParameters().get('accountname') : vr.KS_Account__r.Name;
        accountExteralId = vr.KS_Account__r.KS_External_Id__c == null ? ApexPages.currentPage().getParameters().get('accountexternalid') : vr.KS_Account__r.KS_External_Id__c ;
        accountYTD = vr.KS_Account__r.YTD__c == null ? ApexPages.currentPage().getParameters().get('accountytd') : String.valueOf(vr.KS_Account__r.YTD__c);
        accountVentasYTD = vr.KS_Account__r.Ventas_YTD_1_batch__c == null ? ApexPages.currentPage().getParameters().get('accountventasytd1batch') : String.valueOf(vr.KS_Account__r.Ventas_YTD_1_batch__c);

        KS_ABS_Pipe_and_Fittings = vr.KS_ABS_Pipe_and_Fittings__c == null ? ApexPages.currentPage().getParameters().get('KS_ABS_Pipe_and_Fittings__c') : vr.KS_ABS_Pipe_and_Fittings__c;
        KS_Chemicals = vr.KS_Chemicals__c == null ? ApexPages.currentPage().getParameters().get('KS_Chemicals__c') : vr.KS_Chemicals__c;
        KS_Domestic_Pumps = vr.KS_Domestic_Pumps__c == null ? ApexPages.currentPage().getParameters().get('KS_Domestic_Pumps__c') : vr.KS_Domestic_Pumps__c;
        KS_Dosing_Equipment = vr.KS_Dosing_Equipment__c == null ? ApexPages.currentPage().getParameters().get('KS_Dosing_Equipment__c') : vr.KS_Dosing_Equipment__c;
        KS_Enclosures = vr.KS_Enclosures__c == null ?  ApexPages.currentPage().getParameters().get('KS_Enclosures__c') : vr.KS_Enclosures__c;
        KS_Environmental_Control = vr.KS_Environmental_Control__c == null ? ApexPages.currentPage().getParameters().get('KS_Environmental_Control__c') : vr.KS_Environmental_Control__c;
        KS_Gas_and_Oil_Heaters = vr.KS_Gas_and_Oil_Heaters__c == null ?ApexPages.currentPage().getParameters().get('KS_Gas_and_Oil_Heaters__c') : vr.KS_Gas_and_Oil_Heaters__c;
        KS_Heat_Pumps = vr.KS_Heat_Pumps__c == null ? ApexPages.currentPage().getParameters().get('KS_Heat_Pumps__c') : vr.KS_Heat_Pumps__c;
        KS_Portable_Spas = vr.KS_Portable_Spas__c == null ?  ApexPages.currentPage().getParameters().get('KS_Portable_Spas__c') : vr.KS_Portable_Spas__c;
        KS_Safety_Covers = vr.KS_Safety_Covers__c == null ? ApexPages.currentPage().getParameters().get('KS_Safety_Covers__c') : vr.KS_Safety_Covers__c;
        KS_Slatted_Covers = vr.KS_Slatted_Covers__c == null ? ApexPages.currentPage().getParameters().get('KS_Slatted_Covers__c') : vr.KS_Slatted_Covers__c;
        KS_Connectivity = vr.KS_Connectivity__c == null ? ApexPages.currentPage().getParameters().get('KS_Connectivity__c') : vr.KS_Connectivity__c;
        KS_SWC = vr.KS_SWC__c == null ? ApexPages.currentPage().getParameters().get('KS_SWC__c') : vr.KS_SWC__c;
        KS_Automatic_Cleaners = vr.KS_Automatic_Cleaners__c == null ? ApexPages.currentPage().getParameters().get('KS_Automatic_Cleaners__c') : vr.KS_Automatic_Cleaners__c;
        KS_Domestic_Filters = vr.KS_Domestic_Filters__c == null ? ApexPages.currentPage().getParameters().get('KS_Domestic_Filters__c') : vr.KS_Domestic_Filters__c;
        KS_Commercial_Pumps = vr.KS_Commercial_Pumps__c == null ? ApexPages.currentPage().getParameters().get('KS_Commercial_Pumps__c') : vr.KS_Commercial_Pumps__c;
        KS_Commercial_Filters = vr.KS_Commercial_Filters__c == null ? ApexPages.currentPage().getParameters().get('KS_Commercial_Filters__c') : vr.KS_Commercial_Filters__c;
        KS_Liners = vr.KS_Liners__c == null ? ApexPages.currentPage().getParameters().get('KS_Liners__c') : vr.KS_Liners__c;
        KS_On_site_lining = vr.KS_On_Site_lining__c == null ? ApexPages.currentPage().getParameters().get('KS_On_Site_Lining__c') : vr.KS_On_Site_Lining__c;




        System.debug('-- JCAR -- KS_VisitReport_Controller -- -- -- getVisitReportByID ' + vr);

        String pdfName = getPDFName(vr);
        //pdfName = pdfName.replaceAll('&', '&amp;');
        //pdfName = pdfName.replaceAll('*','&ast;');
        //pdfName = pdfName.replaceAll('/', '&sol;');
        //pdfName = pdfName.replaceAll(']', '&rbrack;');

        Apexpages.currentPage().getHeaders().put('content-disposition',
                        'inline; filename=' + pdfName.replaceAll(',', ''));
    }

    public static KS_Visit_Report__c getVisitReportByID(KS_Visit_Report__c visRep) {
        System.debug('-- JCAR -- getVisitReportByID -- -- -- KS_Visit_Report__c ' + visRep);
        if (visRep == null) {
            return new KS_Visit_Report__c();
        }
        if (visRep.ID == null) {
            return visRep;
        }

        try {
            KS_Visit_Report__c vr = [SELECT Id, Owner.Username, KS_Account__r.Name, KS_Contact__r.Name,
                    KS_Comments__c,KS_Account__r.Ventas_YTD_1_batch__c, KS_Account__r.YTD__c,
                    KS_Date_of_visit__c, KS_Check_In__r.Name, KS_Slatted_Covers__c,
                    KS_Environmental_Control__c, KS_Safety_Covers__c, KS_Heat_Pumps__c,KS_Liners__c,KS_On_Site_Lining__c,
                    KS_Gas_and_Oil_Heaters__c, KS_Chemicals__c, KS_Automatic_Cleaners__c,KS_Commercial_Pumps__c,KS_Commercial_Filters__c,
                    KS_Dosing_Equipment__c, KS_ABS_Pipe_and_Fittings__c, KS_Portable_Spas__c,
                    KS_Domestic_Pumps__c, KS_Domestic_Filters__c, KS_Enclosures__c, KS_SWC__c,KS_Connectivity__c, Name, CreatedBy.Name, KS_Account__r.KS_External_Id__c,
                    CreatedDate, KS_Account__r.KS_141_o_047__c, KS_Check_In__c, Lead__c, Lead__r.Name, KS_Completed__c
                    FROM KS_Visit_Report__c where id = :visRep.ID];
            System.debug('Query! ----------------' + vr);
            return vr;
        } catch (Exception ex) {
            System.debug('PETE ------------' + ex);
            return visRep;
        }
    }


    public PageReference attachPDF() {
        System.debug('Axel------------------ Estado completed - ' + vr.KS_Completed__c);
        if (vr.KS_Completed__c) {
            //generate and attach the PDF document

            PageReference pdfPage =
                    Page.KS_VisitReport_VF; //create a page reference to our pdfDemo Visualforce page, which was created from the post http://www.interactiveties.com/blog/2015/render-visualforce-pdf.php
            pdfPage.getParameters().put('id',
                    vr.id);// replace Id with the correct parameter if you are using other param.
            Blob pdfBlob = pdfPage.getContentAsPDF();

            createPDF(vr, pdfBlob);
            /*
             * Map<String, Object> params = new Map<String, Object>();
             * params.put('Var_VisitReportId',add.id);
             * Flow.Interview flowThis = Flow.Interview.createInterview('UK_Content_Distribution_of_Visit_Reports', params);
             * flowThis.start();
             */

            //insert attach; //insert the attachment

            //redirect the user to reports when i have 2 buttons Attach or generate and this is attach
            // PageReference pageWhereWeWantToGo = new ApexPages.StandardController(add).view(); //we want to redirect the User back to the Account detail page
            //pageWhereWeWantToGo.setRedirect(true); //indicate that the redirect should be performed on the client side
            //return pageWhereWeWantToGo; //send the User on their way
            System.debug('Axel------------------ Estado si completed - ' + vr.KS_Completed__c);
            return pdfPage;
        } else {
            System.debug('Axel------------------ Estado no completed - ' + vr.KS_Completed__c);
            PageReference error = Page.KS_NO_Completed;
            error.getParameters().put('nooverride', '1');
            return error; //send the User on their way
        }
    }

    public PageReference populatePDF() {
        String look_id = ApexPages.currentPage().getParameters().get('id');
        if (look_id != null) {
            KS_Visit_Report__c visitReport = new KS_Visit_Report__c(Id = look_id);
            vr = getVisitReportByID(visitReport);
        }
        return null;
    }

    @InvocableMethod(label = 'Create Visit Report PDF' description = 'Creates PDF Content Document for Visit Report')
    public static void invokableAttachPDF(List<KS_Visit_Report__c> visitReportLst) {

        System.debug('-- JCAR -- invokableAttachPDF -- -- -- visitReportLst ' + visitReportLst);

        if (visitReportLst == NULL) {
            return;
        }
        if (visitReportLst.isEmpty()) {
            return;
        }
        KS_Visit_Report__c visRep = visitReportLst.get(0);

        System.debug('-- JCAR -- invokableAttachPDF -- -- -- visRep ' + visRep);

        visRep = getVisitReportByID(visRep);

        System.debug('-- JCAR -- invokableAttachPDF -- -- -- getVisitReportByID ' + visRep);

        //generate and attach the PDF document
        PageReference pdfPage =
                Page.KS_VisitReport_VF; //create a page reference to our pdfDemo Visualforce page, which was created from the post http://www.interactiveties.com/blog/2015/render-visualforce-pdf.php
        pdfPage.getParameters().put('id',
                visRep.ID);// replace Id with the correct parameter if you are using other param.

        pdfPage.getParameters().put('createdby', visRep.CreatedBy.Name);
        pdfPage.getParameters().put('createddate', String.valueOf(visRep.CreatedDate));
        pdfPage.getParameters().put('dateofvisit', String.valueOf(visRep.KS_Date_of_visit__c));
        pdfPage.getParameters().put('name', visRep.Name);
        pdfPage.getParameters().put('comments', visRep.KS_Comments__c);
        pdfPage.getParameters().put('contactname', visRep.KS_Contact__r.Name);
        pdfPage.getParameters().put('leadname', visRep.Lead__r.Name);
        pdfPage.getParameters().put('accountname', visRep.KS_Account__r.Name);
        pdfPage.getParameters().put('accountexternalid', visRep.KS_Account__r.KS_External_Id__c);
        pdfPage.getParameters().put('accountytd', String.valueOf(visRep.KS_Account__r.YTD__c));
        pdfPage.getParameters().put('accountventasytd1batch',
                String.valueOf(visRep.KS_Account__r.Ventas_YTD_1_batch__c));
        pdfPage.getParameters().put('KS_ABS_Pipe_and_Fittings__c', visRep.KS_ABS_Pipe_and_Fittings__c);
        pdfPage.getParameters().put('KS_Chemicals__c', visRep.KS_Chemicals__c);
        pdfPage.getParameters().put('KS_Domestic_Pumps__c', visRep.KS_Domestic_Pumps__c);
        pdfPage.getParameters().put('KS_Dosing_Equipment__c', visRep.KS_Dosing_Equipment__c);
        pdfPage.getParameters().put('KS_Enclosures__c', visRep.KS_Enclosures__c);
        pdfPage.getParameters().put('KS_Environmental_Control__c', visRep.KS_Environmental_Control__c);
        pdfPage.getParameters().put('KS_Gas_and_Oil_Heaters__c', visRep.KS_Gas_and_Oil_Heaters__c);
        pdfPage.getParameters().put('KS_Heat_Pumps__c', visRep.KS_Heat_Pumps__c);
        pdfPage.getParameters().put('KS_Portable_Spas__c', visRep.KS_Portable_Spas__c);
        pdfPage.getParameters().put('KS_Safety_Covers__c', visRep.KS_Safety_Covers__c);
        pdfPage.getParameters().put('KS_Slatted_Covers__c', visRep.KS_Slatted_Covers__c);
        pdfPage.getParameters().put('KS_Connectivity__c', visRep.KS_Connectivity__c);
        pdfPage.getParameters().put('KS_SWC__c', visRep.KS_SWC__c);
        pdfPage.getParameters().put('KS_Automatic_Cleaners__c', visRep.KS_Automatic_Cleaners__c);
        pdfPage.getParameters().put('KS_Domestic_Filters__c', visRep.KS_Domestic_Filters__c);
        pdfPage.getParameters().put('KS_Commercial_Pumps__c', visRep.KS_Commercial_Pumps__c);
        pdfPage.getParameters().put('KS_Commercial_Filters__c', visRep.KS_Commercial_Filters__c);
        pdfPage.getParameters().put('KS_Liners__c', visRep.KS_Liners__c);
        pdfPage.getParameters().put('KS_On_Site_Lining__c', visRep.KS_On_Site_Lining__c);


        pdfPage.setRedirect(true);


        System.debug('-- JCAR -- invokableAttachPDF -- -- -- pdfPage ' + pdfPage);
        if (visRep.KS_Completed__c) {
            System.debug('------------------' + visRep);
            try {
                Blob pdfBlob =
                        pdfPage.getContentAsPDF(); //get the output of the page, as displayed to a user in a browser
                createPDF(visRep, pdfBlob);
            } catch (Exception e) {
                System.debug('Bhupi ------------' + e);
            }
        }

    }

    public static void createPDF(KS_Visit_Report__c visRep, Blob pdfBlob) {

        System.debug('-- JCAR -- createPDF -- -- -- KS_Visit_Report__c ' + visRep);
        System.debug('-- JCAR -- createPDF -- -- -- Blob ' + JSON.serialize(pdfBlob));

        String pdfName = getPDFName(visRep);
        Attachment attach =
                new Attachment(ParentId = visRep.Id, Name = pdfName, body = pdfBlob); //create the attachment object
        ContentDocumentLink cdl = KS_ContentDocument_Utils.setAttachmentToContentDocument(attach);

        System.debug('-- JCAR -- createPDF -- -- -- ContentDocumentLink ' + cdl);
    }

    public static String getPDFName(KS_Visit_Report__c visRep) {

        if (visRep == null) {
            return 'VisitReport';
        }

        //Date d = visRep.KS_Date_of_visit__c;
        //String dt = DateTime.newInstance(d.year(),d.month(),d.day()).format('dd/MM/YYYY');
        String dt = DateTime.now().format('dd/MM/YYYY');

        String name = visRep.KS_Account__r.Name + '_' + dt;
        String name2 = visRep.KS_Contact__r.Name + '_' + dt;
        String pdfName = visRep.KS_Account__r.Name != null ? name : name2;

        if (visRep.KS_Account__c != null && visRep.KS_Account__r.Name == null) {

            pdfName = [SELECT Name FROM Account WHERE ID = :visRep.KS_Account__c].Name;

        } else if (visRep.KS_Account__c == null && visRep.KS_Contact__c != null && visRep.KS_Contact__r.Name == null) {

            pdfName = [SELECT Name FROM Contact WHERE ID = :visRep.KS_Contact__c].Name;
        }
        return pdfName + '.pdf';
    }

    public PageReference completeReport() {
        if (!vr.KS_Completed__c) {
            vr.KS_Completed__c = true;
        }
        //attachPDF(); // ahora lo lanza el process
        try {
            update vr;
        } catch (Exception e) {
            System.debug('Bhupi ------------ ' + vr);
            System.debug('Bhupi ------------ ' + e);
        }
        PageReference pdfPage = Page.KS_VisitReport_VF;
        //devolvemos el pdfPage generado antes para mostrarlo en pantalla
        return pdfPage;
    }
}