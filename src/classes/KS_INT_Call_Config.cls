@RestResource(urlMapping = '/CallConfig/*')
global class KS_INT_Call_Config {

	@HttpGet
	global static callConfig getConfig() {
		callConfig cfg = new callConfig();
		KS_Calls_Configuration__mdt config = [Select KS_username__c, KS_password__c, KS_token__c, KS_client_id__c, KS_client_secret__c, KS_logo_base64__c
		                                      from KS_Calls_Configuration__mdt where DeveloperName = 'GENERAL'];
		if (config != null){
			cfg.username = config.KS_username__c;
			cfg.pass = config.KS_password__c;
			cfg.token = config.KS_token__c;
			cfg.clientId = config.KS_client_id__c;
			cfg.clientSecret = config.KS_client_secret__c;
			cfg.logo = config.KS_logo_base64__c;
		}

		return cfg;

	}

	global class callConfig {
		public string username { get; set; }
		public string pass { get; set; }
		public string token { get; set; }
		public string clientSecret { get; set; }
		public string clientId { get; set; }
		public string logo { get; set; }
	}

}