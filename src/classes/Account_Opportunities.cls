public class Account_Opportunities {
@AuraEnabled
public static List<KS_Alarm_and_Opportunity__c> getOpps(Id recordId) {
   return [Select id,KS_Business_Unit__c, KS_Familia_estrat_gica__c, KS_Value__c, KS_Alarm_measure__c From KS_Alarm_and_Opportunity__c Where KS_Account__c = :recordId AND KS_Tipo_de_entrada__c = 'Opportunity'];

}

}