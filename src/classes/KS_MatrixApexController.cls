global class KS_MatrixApexController {
    
    @AuraEnabled
    global static void voidMethod(){}
    
    @AuraEnabled
    global static List<Account> getAccountList() {
        return [SELECT 
                Id, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone 
                FROM Account ORDER BY createdDate ASC];
    }
    
    @AuraEnabled
    global static List<KS_Engagement__c> getEngagementList(String matrix) {
        List<KS_Engagement__c> engLst = new List<KS_Engagement__c>();
        engLst = [SELECT ID, Name, KS_Customer_Code__c, KS_Account__c, KS_Matrix__c, KS_Business_Unit__c, KS_Label__c, 
                  KS_Engagement__c, KS_FLUIDRA_SALES__c, KS_New_Product_Sales__c, KS_Customer_Potential__c, KS_DSO__c, 
                  KS_ERP_Code__c, KS_Fecha__c, KS_Fluidra_YTD__c, KS_New_Product_YTD__c, KS_Product_Mix__c, KS_SOW__c, KS_TAM12__c, KS_VABC__c, KS_YTD__c, 
                  KS_Total_Points_formula__c, KS_Points_DSO__c, KS_Points_Fluidra__c, KS_Points_Mix__c, KS_Points_New_Product__c, KS_Points_SOW__c
                  FROM KS_Engagement__c
                  WHERE KS_Matrix__c = :matrix];
        return engLst;
    }
    
    @AuraEnabled
    global static List<List<List<String>>> buildPickList() {
        List<List<List<String>>> picklist = new List<List<List<String>>>();
        Set<ID> userIDs = new Set<ID>(); userIDs.add(UserInfo.getUserId());
            
        //piclist de bussiness units disponibles
        List<List<String>> bsuList = getPickListBussUnit();
        // picklist de familia estratégica (de Sales)
        List<List<String>> famList = getPickListFamilia();
        // picklist de usuarios posibles
        List<List<String>> usrList = getPickListUser(userIDs);
        
        System.debug('-- JCAR -- buildPickList bsuList ' + bsuList);
        System.debug('-- JCAR -- buildPickList famList ' + famList);
        System.debug('-- JCAR -- buildPickList usrList ' + usrList);
        picklist.add(bsuList);
        picklist.add(famList);
        picklist.add(usrList);
        return picklist;
    }
    global static List<List<String>> getPickListBussUnit() {
        List<List<String>> bsuList = new List<List<String>>();
        bsuList.add(new List<String>{'ALL','ALL BU'});
        for(AggregateResult groupBy : [SELECT KS_Business_Unit__c FROM KS_Engagement__c GROUP BY KS_Business_Unit__c]) {
            String buString = (String)groupBy.get('KS_Business_Unit__c');
            if (buString != null && buString.trim() != '') { bsuList.add(new List<String>{buString,buString}); }
        }
        return bsuList;
    }
    global static List<List<String>> getPickListFamilia() {
        List<List<String>> famList = new List<List<String>>();
        famList.add(new List<String>{'ALL','ALL FAMILIES'});
        Schema.DescribeFieldResult fieldResult = Product2.KS_Strategical_Family__c.getDescribe();
        for( Schema.PicklistEntry pickListVal : fieldResult.getPicklistValues()){
            List<String> lst = new List<String>();
            lst.add(pickListVal.getLabel());
            lst.add(pickListVal.getValue());
            famList.add(lst);
        }
        return famList;
    }
    global static List<List<String>> getPickListUser(Set<ID> userIDs) {
        List<List<String>> usrList = new List<List<String>>();

        Boolean isAdmin = false;
        for (Profile prof : [SELECT ID FROM Profile WHERE 
                             Name LIKE 'System Administrator' OR
                             Name LIKE 'Administrador del sistema'
                             limit 1]) {
            isAdmin = UserInfo.getProfileId() == prof.ID;
        }
        
        Set<String> codResSet = new Set<String>();
        for (AggregateResult agg :
             [SELECT KS_Responsable1__c , KS_Responsable2__c , KS_Responsable3__c 
              FROM Account
              WHERE KS_Responsable1__c != NULL OR KS_Responsable2__c != NULL OR KS_Responsable3__c != NULL
              GROUP BY KS_Responsable1__c , KS_Responsable2__c , KS_Responsable3__c]) {
                  if (agg.get('KS_Responsable1__c') != null) { codResSet.add((String)agg.get('KS_Responsable1__c')); }
                  if (agg.get('KS_Responsable2__c') != null) { codResSet.add((String)agg.get('KS_Responsable2__c')); }
                  if (agg.get('KS_Responsable3__c') != null) { codResSet.add((String)agg.get('KS_Responsable3__c')); }
              }
        
        Set<ID> yaesta = new Set<ID>();
        for (KS_Identidades_Usuario__c idUS : 
             [SELECT KS_Sales_rep_code__c,KS_Usuario_Salesforce__c,KS_Usuario_Salesforce__r.Name 
              FROM KS_Identidades_Usuario__c WHERE KS_Sales_rep_code__c in :codResSet]) {
                  if (!yaesta.contains(idUS.KS_Usuario_Salesforce__c) && (idUS.KS_Usuario_Salesforce__c == UserInfo.getUserId() || isAdmin)) {
                      List<String> lst = new List<String>();
                      lst.add(/*idUS.KS_Sales_rep_code__c*/idUS.KS_Usuario_Salesforce__c);
                      lst.add(/*idUS.KS_Sales_rep_code__c+' - '+*/idUS.KS_Usuario_Salesforce__r.Name);
                      usrList.add(lst);
                      yaesta.add(idUS.KS_Usuario_Salesforce__c);
                  }
              }
        if (usrList.isEmpty()) { usrList.add(new List<String>{'',''}); }
        return usrList;
    }
    
    @AuraEnabled
    global static KS_Matriz__c getTotalMatriz(String matrix) 
    { return getTotalMatrizFiltrado(matrix, '', '', ''); }
    @AuraEnabled
    global static KS_Matriz__c getTotalMatrizFiltrado(String matrix, String matrixBSU, String matrixFAM, String matrixUSR) 
    { return getTotalMatrizMethod(matrix, matrixBSU, matrixFAM, matrixUSR, false); }
    
    global static KS_Matriz__c getTotalMatrizMethod(String matrix, String matrixBSU, String matrixFAM, String matrixUSR, Boolean mockValues) {
        //return new KS_Matriz__c();
        MatrizWrapper matriz = new MatrizWrapper();

        //if (matrixUSR == null || matrixUSR != null && matrixUSR.trim() == '') { matrixUSR = UserInfo.getUserId(); }
        Boolean haveBussU = matrixBSU != null && matrixBSU.trim() != '' && matrixBSU.trim() != 'ALL';
        Boolean haveFamil = matrixFAM != null && matrixFAM.trim() != '' && matrixFAM.trim() != 'ALL';
        Boolean haveUsuar = matrixUSR != null && matrixUSR.trim() != '' && matrixUSR.trim() != 'ALL';
        if (!haveBussU && !haveFamil && !haveUsuar) { return new KS_Matriz__c(); }
             
        System.debug('-- JCAR -- getTotalMatrizMethod matrix ' + matrix);
        System.debug('-- JCAR -- getTotalMatrizMethod matrixBSU ' + matrixBSU);
        System.debug('-- JCAR -- getTotalMatrizMethod matrixFAM ' + matrixFAM);
        System.debug('-- JCAR -- getTotalMatrizMethod matrixUSR ' + matrixUSR);

        Set<String> matrixREP = new Set<String>();
        if (haveUsuar) {
            for (KS_Identidades_Usuario__c idUS : 
                 [SELECT KS_Sales_rep_code__c,KS_Usuario_Salesforce__c FROM KS_Identidades_Usuario__c
                  WHERE KS_Usuario_Salesforce__c = :matrixUSR OR KS_Sales_rep_code__c = :matrixUSR]) {
                              matrixREP.add(idUS.KS_Sales_rep_code__c);
                  }
        }

        // QUERY DE ENGAGEMENT
        String QRYEngage = 
            'SELECT KS_Account__c, KS_Matrix__c, '
            + ' sum(KS_Engagement__c) engagement, sum(KS_FLUIDRA_SALES__c) fluidraSales, sum(KS_New_Product_Sales__c) newProductSales, '
            + ' sum(KS_Customer_Potential__c) customerPotential, sum(KS_Fluidra_YTD__c) fluidraYTD, sum(KS_New_Product_YTD__c) newProductoYTD,  '
            + ' sum(KS_Product_Mix__c) productMix, sum(KS_SOW__c) sow, sum(KS_TAM12__c) tam12, sum(KS_Total_Points_formula__c) totalPoints, ' 
            + ' sum(KS_YTD__c) ytd FROM KS_Engagement__c WHERE KS_Matrix__c = \''+matrix+'\' ';
        if (matrixBSU != null && matrixBSU.trim() != '' && matrixBSU.trim() != 'ALL') { QRYEngage += ' AND KS_Business_Unit__c = \''+matrixBSU+'\' '; }
        if (matrixUSR != null && matrixUSR.trim() != '' && matrixUSR.trim() != 'ALL') {
            /*QRYEngage += ' AND ( KS_Account__r.KS_Responsable1__c = \''+matrixUSR+'\'' 
                + ' OR KS_Account__r.KS_Responsable2__c = \''+matrixUSR+'\'' 
                + ' OR KS_Account__r.KS_Responsable3__c = \''+matrixUSR+'\' ) ';*/
            QRYEngage += ' AND ( KS_Account__r.KS_Responsable1__c IN :matrixREP' 
                + ' OR KS_Account__r.KS_Responsable2__c IN :matrixREP' 
                + ' OR KS_Account__r.KS_Responsable3__c IN :matrixREP ) ';
        }
        QRYEngage += ' GROUP BY KS_Account__c, KS_Matrix__c ORDER BY KS_Account__c ';
        
        List<AggregateResult> aggLst = Database.query(QRYEngage);
        System.debug('-- JCAR -- QRYEngage ' + QRYEngage);
        System.debug('-- JCAR -- engMap size ' + aggLst.size());
        if (aggLst.size()==0) { return new KS_Matriz__c(); }
                
        Map<ID,KS_Engagement__c> engMap = new Map<ID,KS_Engagement__c>();
        for (AggregateResult agg : aggLst) {
            KS_Engagement__c eng = new KS_Engagement__c();
            eng.KS_Customer_Potential__c = (Decimal)agg.get('customerPotential');
            eng.KS_TAM12__c = (Decimal)agg.get('tam12');
            eng.KS_Account__c =  (String)agg.get('KS_Account__c');
            engMap.put(eng.KS_Account__c, eng);
            if (eng.KS_Customer_Potential__c != null) { matriz.KS_Potential += eng.KS_Customer_Potential__c; }
            if (eng.KS_TAM12__c != null) { matriz.KS_TTM += eng.KS_TAM12__c; }
        }
        if (matriz.KS_Potential > 0 && matriz.KS_TTM > 0) {
            matriz.KS_Coverage = matriz.KS_TTM/matriz.KS_Potential;
        }

        String maxFecha = (Date.today().year()) + '-01-01';
        String minFecha = (Date.today().year()-1) + '-01-01';
        
        //matriz.KS_NumberClients = engMap.keySet().size();
        
        Set<String> cuentasCount = new Set<String>();
        // QUERY DE SALES
        List<AggregateResult> aggSalesOLD = getSalesQRY(engMap.isEmpty()? new Set<ID>() : engMap.keySet(), ' KS_Fecha__c >= '+minFecha+' AND KS_Fecha__c < '+maxFecha+' ', matrixFAM, matrixREP);
        for (AggregateResult agg : aggSalesOLD) {
            Decimal coste = (Decimal)agg.get('costes');
            Decimal venta = (Decimal)agg.get('ventas');
            if (venta != null) { matriz.KS_YTD_1+=venta; } if (coste != null) { matriz.KS_YTD_Cost_1+=coste; }
            String acc = (String)agg.get('KS_Account__c'); if (acc != null) { cuentasCount.add(acc); }
        }

        List<AggregateResult> aggSalesNEW = getSalesQRY(engMap.isEmpty()? new Set<ID>() : engMap.keySet(), ' KS_Fecha__c >= '+maxFecha+' ', matrixFAM, matrixREP);
        for (AggregateResult agg : aggSalesNEW) {
            Decimal coste = (Decimal)agg.get('costes');
            Decimal venta = (Decimal)agg.get('ventas');
            if (venta != null) { matriz.KS_YTD_0+=venta; } if (coste != null) { matriz.KS_YTD_Cost_0+=coste; }
            String acc = (String)agg.get('KS_Account__c'); if (acc != null) { cuentasCount.add(acc); }
        }
        
        System.debug('-- JCAR -- cuentasCount ' + cuentasCount);
        matriz.KS_NumberClients = cuentasCount.size();
        
        if (mockValues) { 
            matriz.mockValues(20, 2);
            System.debug('-- JCAR -- getTotalMatriz ' + matriz);
        }
        
        return matriz.returnMatriz();
    }
    global static List<AggregateResult> getSalesQRY(Set<ID> accIDSet, String fechaQRY, String matrixFAM, Set<String> matrixREP) {
        
        //if (matrixUSR == UserInfo.getUserId()) { matrixUSR = null; }
        String QRYSales = 
            'SELECT KS_Account__c, sum(KS_Margen__c) margen, sum(KS_Sales_LC__c) ventas, sum(KS_Cost_LC__c) costes '
            + ' FROM KS_Sales__c ';
        IF (accIDSet.size()>0) {
            QRYSales += ' WHERE KS_Account__c IN (';
            Integer count = 0;
            FOR (ID accID : accIDSet) {
                QRYSales += '\''+accID+'\''; if (count<accIDSet.size()-1) { QRYSales += ','; }
                count++;
            }
        }
        Boolean haveFecha = fechaQRY != null && fechaQRY.trim() != '';
        Boolean haveFamil = matrixFAM != null && matrixFAM.trim() != '' && matrixFAM.trim() != 'ALL';
        Boolean haveUsuar = matrixREP != null && !matrixREP.isEmpty();
        
        if (haveFecha) { QRYSales += (accIDSet.isEmpty()? ' WHERE ' : ') AND ') + fechaQRY; }
        if (haveFamil) { QRYSales += (haveFecha?' AND ':' WHERE ') + '  KS_Producto__r.KS_Strategical_Family__c = \''+matrixFAM+'\' '; }
        if (haveUsuar) {
            /*QRYSales += (haveFecha||haveFamil?' AND ':' WHERE ') + '  ( KS_Account__r.KS_Responsable1__c = \''+matrixUSR+'\'' 
                + ' OR KS_Account__r.KS_Responsable2__c = \''+matrixUSR+'\'' 
                + ' OR KS_Account__r.KS_Responsable3__c = \''+matrixUSR+'\' ) ';*/
            QRYSales += (haveFecha||haveFamil?' AND ':' WHERE ') + '  ( KS_Account__r.KS_Responsable1__c IN :matrixREP' 
                + ' OR KS_Account__r.KS_Responsable2__c IN :matrixREP' 
                + ' OR KS_Account__r.KS_Responsable3__c IN :matrixREP ) ';
        }
        QRYSales += ' GROUP BY KS_Account__c ORDER BY KS_Account__c ';
        System.debug('-- JCAR -- QRYSales ' + QRYSales);
        return Database.query(QRYSales);
    }
    
    global Class MatrizWrapper {
        Decimal KS_NumberClients {get;set;}
        Decimal KS_Coverage {get;set;}
        Decimal KS_Potential {get;set;}
        Decimal KS_TTM {get;set;}
        Decimal KS_YTD_0 {get;set;}
        Decimal KS_YTD_Cost_0 {get;set;}
        Decimal KS_YTD_GM_0 {get;set;}
        Decimal KS_YTD_1 {get;set;}
        Decimal KS_YTD_Cost_1 {get;set;}
        Decimal KS_YTD_GM_1 {get;set;}
        
        public MatrizWrapper() {
            KS_NumberClients = 0; KS_Coverage = 0;
            KS_Potential = 0; KS_TTM = 0;
            KS_YTD_0 = 0; KS_YTD_Cost_0 = 0; KS_YTD_GM_0 = 0;
            KS_YTD_1 = 0; KS_YTD_Cost_1 = 0; KS_YTD_GM_1 = 0;
        }
        public void mockValues(Integer max, Integer min) {
            KS_NumberClients = Math.random()*(max-min)+min;
            KS_Coverage = Math.random()*(max-min)+min;
            KS_Potential = Math.random()*(max-min)+min;
            KS_TTM = Math.random()*(max-min)+min;
            KS_YTD_0 = Math.random()*(max-min)+min;
            KS_YTD_GM_0 = Math.random()*(max-min)+min;
            KS_YTD_1 = Math.random()*(max-min)+min;
            KS_YTD_GM_1 = Math.random()*(max-min)+min;
        }
        
        
        public KS_Matriz__c returnMatriz() {
            
            if (KS_YTD_0>0) { KS_YTD_GM_0 = (( KS_YTD_0 - KS_YTD_Cost_0 ) / KS_YTD_0)*100; }
            if (KS_YTD_1>0) { KS_YTD_GM_1 = (( KS_YTD_1 - KS_YTD_Cost_1 ) / KS_YTD_1)*100; }
            
            KS_Matriz__c mat = new KS_Matriz__c();
            mat.KS_NumberClients__c = formatValue(KS_NumberClients, false, false);
            mat.KS_Coverage__c = formatValue(KS_Coverage, true, true);
            mat.KS_Potential__c = formatValue(KS_Potential, false, false);
            mat.KS_TTM__c = formatValue(KS_TTM, false, false);
            mat.KS_YTD_0__c = formatValue(KS_YTD_0, false, false);
            mat.KS_YTD_GM_0__c = formatValue(KS_YTD_GM_0, true, true);
            mat.KS_YTD_1__c = formatValue(KS_YTD_1, false, false);
            mat.KS_YTD_GM_1__c = formatValue(KS_YTD_GM_1, true, true);
            System.debug('-- JCAR -- returnMatriz ' + mat);
            return mat;
        }
        public String formatValue(Decimal value, Boolean decimals, Boolean percent) {
            String valueS = '';
            valueS = decimals ? value.format() : value.round().format();
            return percent ? valueS+'%' : valueS;
        }
    }
}