@isTest 
public class F_Trigger_OpportunityTest {
    
    @isTest
    private static void F_Trigger_Opportunity() {
        Opportunity ope = new Opportunity();
        ope.Name = 'Test de Oportunidad';
        ope.RecordtypeId = [ select id from recordType where SobjectType = 'Opportunity' and DeveloperName = 'FES'].Id;
        ope.AccountId = '0011v000020lOptAAE';
        ope.CloseDate = Date.newInstance(2019, 12, 9);
        ope.StageName = 'Lead';
        ope.KS_Tipo_de_oportunidad__c = 'Pool';
        ope.Amount = 223143243;
        ope.CurrencyIsoCode = 'EUR';
        ope.Date__c = Date.newInstance(2018, 12, 9);

        insert ope; 

        Opportunity ope2 = new Opportunity();
        ope2.Name = 'Test de Oportunidad2';
        ope2.RecordtypeId = [ select id from recordType where SobjectType = 'Opportunity' and DeveloperName = 'FES'].Id;
        ope2.AccountId = '0011v000020lOptAAE';
        ope2.CloseDate = Date.newInstance(2019, 12, 9);
        ope2.StageName = 'Lead';
        ope2.KS_Tipo_de_oportunidad__c = 'Pool';
        ope2.Amount = 22314354;
        ope2.CurrencyIsoCode = 'EUR';
        ope2.Date__c = Date.newInstance(2018, 12, 9);
            
        insert ope2;    

        Opportunity ope3 = new Opportunity();
        ope3.Name = 'Test de Oportunidad3';
        ope3.RecordtypeId = [ select id from recordType where SobjectType = 'Opportunity' and DeveloperName = 'FES'].Id;
        ope3.AccountId = '0011v000020lOptAAE';
        ope3.CloseDate = Date.newInstance(2019, 12, 9);
        ope3.StageName = 'Lead';
        ope3.KS_Tipo_de_oportunidad__c = 'Pool';
        ope3.Amount = 22314354;
        ope3.CurrencyIsoCode = 'EUR';
        ope3.Date__c = Date.newInstance(2018, 12, 9);
            
        insert ope3;    

        ope.Amount = 5678943;
        ope.Name = 'Nombre mejorado';

        update ope;

        ope2.Amount = 365093;
        ope2.Name = 'Nombre mejorado22';

        update ope2;
        }
}