public with sharing class KS_LLamadas_Trigger extends KS_TriggerController {

	public KS_LLamadas_Trigger(Boolean isAfter, Boolean isBefore, Boolean isDelete, Boolean isInsert, Boolean isUndelete, Boolean isUpdate, List<sObject> lstNewItems, Map<Id, sObject> mapNewItems, List<sObject> lstOldItems, Map<Id, sObject> mapOldItems)
	{
		super(isAfter, isBefore, isDelete, isInsert, isUndelete, isUpdate, lstNewItems, mapNewItems, lstOldItems, mapOldItems);
	}

	protected override void runBeforeInsert() {
		registraLLamada();
	}

	protected override void runBeforeUpdate() {
		registraLLamada();
	}

	private void registraLLamada() {
		List<KS_Llamadas__c> callOldLst = (List<KS_Llamadas__c>) lstOldItems;
		List<KS_Llamadas__c> callLst = (List<KS_Llamadas__c>) lstNewItems;

		for (KS_Llamadas__c call : callLst) {
			//Si ya ha sido tratada, no
			if (call.KS_Tratada__c) continue;
			string telf = '';
			//Busqueda por SOSL de contactos o Leads
			if (call.KS_Telefono_Destino__c != null && call.KS_Telefono_Destino__c.left(2) == '00') {
				telf = call.KS_Telefono_Destino__c.right(call.KS_Telefono_Destino__c.length() - 2);
			}
			else {
				telf = call.KS_Telefono_Destino__c;
			}
			System.debug('*********************** telf: ' + telf);
			//string searchTxt = '*' + telf + '*';
            if (telf == null) { continue; }
			List<List<SObject>> searchObj = [FIND :telf IN PHONE FIELDS RETURNING Contact, Lead];
            
			List<Contact> ctcList = !Test.isRunningTest()? searchObj[0] : [SELECT ID,Phone FROM Contact WHERE Phone = :telf];
			List<Lead> leadList = !Test.isRunningTest()? searchObj[1] : [SELECT ID,Phone FROM Lead WHERE Phone = :telf];

			System.debug('********************** ctcList: ' + ctcList);
			System.debug('********************** leadList: ' + leadList);

			Task t = new Task();
			Boolean crear = false;
			if (ctcList.size() >= 1 && leadList.size() == 0) {
				for (Integer i = 0; i<ctcList.size(); i++) {
					t.WhoId = ctcList[i].Id;
					crear = true;
				}

			} else if (ctcList.size() == 0 && leadList.size()>= 1) {
				for (Integer i = 0; i<leadList.size(); i++) {
					t.WhoId = leadList[i].Id;
					crear = true;
				}
			}
			System.debug('******************** crear: ' + crear);
			t.Subject = 'Call';
			t.Status = 'Completed';
			t.TaskSubtype = 'Call';
			t.CallDurationInSeconds = Integer.valueOf(call.KS_Duracion__c);
			t.CallObject = call.KS_Identificador_llamada__c;
			string aux = '';

			aux = 'LLamada de tipo: ' + call.KS_Tipo_de_llamada__c + ' sobre el numero' + call.KS_Telefono_Destino__c + '.\n\nDuracion de la llamada : ' + call.KS_Duracion__c;
			/*
			  if (call.KS_Tipo_de_llamada__c == 'Saliente') {
			  aux = 'LLamada saliente al numero' + call.KS_Telefono_Destino__c + '.\nDuracion de la llamada : ' + call.KS_Duracion__c;
			  }
			  else if (call.KS_Tipo_de_llamada__c == 'Entrante') {
			  aux = 'LLamada entrante del numero ' + call.KS_Telefono_Destino__c + '.\nDuracion de la llamada : ' + call.KS_Duracion__c;
			  }
			  else if (call.KS_Tipo_de_llamada__c == 'Perdida') {
			  aux = 'LLamada perdida del numero ' + call.KS_Telefono_Destino__c + '.\n';
			  }
			  else{
			  aux = 'LLamada de tipo: ' + call.KS_Tipo_de_llamada__c + ' del numero ' + call.KS_Telefono_Destino__c + '.\n';
              }
              */
            
            t.Description = aux;
            t.ActivityDate = Date.valueOf(call.KS_Inicio_llamada__c);
            if (call.KS_Origen__c != null)
                t.OwnerId = call.KS_Origen__c;
            try {
                if (crear) {
                    insert t;
                    call.KS_Tratada__c = true;
                }
                
            }
            catch(Exception e) {
                System.debug('*********************** e: ' + e.getMessage());
                throw e;
            }
            
            
        }
    }


}