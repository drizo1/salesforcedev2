global class KS_Webservice_Response  
{
    //Listado de atributos para devolver en los mensajes
    public String Status {get; set;}
    public String Message {get;set;}
    public List<sObject> Data {get;set;}
    
    //Check if Object is NULL or Blank
    global static boolean isNotNullOrEmpty(string str)
    {
        return str!=null || !String.isBlank(str); 
    }   
}