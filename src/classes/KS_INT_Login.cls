@RestResource(urlMapping = '/Login/*')
global class KS_INT_Login {


	@HttpPost
	//	public String username { get; set; }
	//	public transient String password { get; set; }
	//	public String LOGIN_DOMAIN { get; set; }
	global static LoginResult Post() {
		KS_Webservice_Response response = new KS_Webservice_Response();
		Credentials c = new Credentials();
		RestRequest req = RestContext.request;
		system.debug('---------- Request: ' + req);

		String jSONRequestBody = req.requestBody.toString().trim();
		system.debug('------- request.body: ' + jSONRequestBody);
		c = (Credentials) JSON.deserialize(jSONRequestBody, Credentials.class);
		if (c.UserName != null && c.Password != null) {
			String username = c.UserName;
			String password = c.Password;
			String logindomain = c.InstanceUrl;
			LoginResult result = doVerify(username, password, logindomain);
			return result;
		} else return null;

	}

	global static LoginResult doVerify(string username, String password, String InstanceUrl) {

		HttpRequest request = new HttpRequest();
		//request.setEndpoint('https://' + LOGIN_DOMAIN + '.salesforce.com/services/Soap/u/22.0');
		request.setEndpoint(InstanceUrl + '/services/Soap/u/22.0');
		request.setMethod('POST');
		request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
		request.setHeader('SOAPAction', '""');
		System.debug('******************* username: ' + username);
		System.debug('******************* password: ' + password);
		System.debug('******************* InstanceUrl: ' + InstanceUrl);
		request.setBody(buildSoapLogin(username, password));

		//basically if there is a loginResponse element, then login succeeded; else there
		//  would be soap fault element after body
		Http X = new Http();
		HttpResponse resp = X.send(request);

		System.debug('**************** resp: ' + resp.getBody());



		final Boolean verified = (new Http()).send(request).getBodyDocument().getRootElement()
		.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/')
		.getChildElement('loginResponse', 'urn:partner.soap.sforce.com') != null;


		LoginResult res = new LoginResult();

		if (verified) {

			List<dom.XmlNode> userInfo = resp.getBodyDocument().getRootElement().getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/').getChildElement('loginResponse', 'urn:partner.soap.sforce.com').getChildElement('result', 'urn:partner.soap.sforce.com').getChildElement('userInfo', 'urn:partner.soap.sforce.com').getChildElements();
			string userId = '', usernam = '', userEmail = '', fullName = '';
			System.debug('******************* userInfo.size(): ' + userInfo.size());
			for (Integer i = 0; i<userInfo.size(); i++) {
				string aux = userInfo[i].getName();
				System.debug('******************* aux: ' + aux);
				if (aux == 'userId') userId = userInfo[i].getText();
				if (aux == 'userName') usernam = userInfo[i].getText();
				if (aux == 'userEmail') userEmail = userInfo[i].getText();
				if (aux == 'userFullName') fullName = userInfo[i].getText();
			}
			System.debug('******************* userId: ' + userId);
			res.Message = 'Correct password!';
			res.UserId = userId;
			res.username = usernam;
			res.email = userEmail;
			res.fullName = fullName;
			res.Status = 'OK';
		} else
		{
			res.Status = 'Error';
			res.Message = '404 - Not Found';
		}
		return res;
	}

	public static String buildSoapLogin(String username, String password) {
		XmlStreamWriter w = new XmlStreamWriter();
		w.writeStartElement('', 'login', 'urn:partner.soap.sforce.com');
		w.writeNamespace('', 'urn:partner.soap.sforce.com');
		w.writeStartElement('', 'username', 'urn:partner.soap.sforce.com');
		w.writeCharacters(username);
		w.writeEndElement();
		w.writeStartElement('', 'password', 'urn:partner.soap.sforce.com');
		w.writeCharacters(password);
		w.writeEndElement();
		w.writeEndElement();

		String xmlOutput =
		'<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Body>'
		+ w.getXmlString()
		+ '</Body></Envelope>';
		w.close();
		return xmlOutput;
	}

	global Class Credentials {
		public string UserName { get; set; }
		public String Password { get; set; }
		public String InstanceUrl { get; set; }
	}

	global Class LoginResult {
		public String Status { get; set; }
		public String Message { get; set; }
		public String UserId { get; set; }
		public string username { get; set; }
		public string email { get; set; }
		public string fullName { get; set; }
	}
}