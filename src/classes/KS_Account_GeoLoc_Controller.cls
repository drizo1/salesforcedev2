public class KS_Account_GeoLoc_Controller {
    
    @AuraEnabled
    public static void nothing() {}
    
    @AuraEnabled
    public static List<Account> searchCuentas(/*Decimal userLat, Decimal userLon,*/ String provincia) {
        
        System.debug('-- JCAR -- searchCuentas -- -- - -- -- -- - provincia - ' + provincia);
        if (provincia == null) {return /*data.markerMap.values()*/ new List<Account>();} 
        if (provincia.trim() == '') {return /*data.markerMap.values()*/ new List<Account>();}
        String criteria = '%'+provincia+'%';criteria = criteria.replace(' ', '%');
        
        List<Account> accs =  [SELECT ID, Name, BillingCity, BillingState, BillingPostalCode, BillingStreet, KS_Segmento__c,
                               BillingLatitude, BillingLongitude, KS_Location__Latitude__s, KS_Location__Longitude__s FROM Account
                               WHERE ( (BillingLatitude != NULL AND BillingLongitude != NULL) or (KS_Location__Latitude__s != NULL AND KS_Location__Longitude__s != NULL) )
                               AND (BillingState LIKE :criteria OR BillingCity LIKE :criteria)
                               ORDER BY Name ASC LIMIT 50];
        System.debug('-- JCAR -- searchCuentas -- -- - -- -- -- - ' + accs.size() + ' - CUENTAS - ' + accs);
        return accs;
    }
    
    public static List<Account> nearCuentasOLD(Decimal userLat, Decimal userLon) {
        
        System.debug('-- JCAR -- nearCuentas -- -- - -- -- -- - userLatitude - ' + userLat);
        System.debug('-- JCAR -- nearCuentas -- -- - -- -- -- - userLongitude - ' + userLon);
        
        Decimal maxRadius = KS_Account_GeoLoc_Controller.maxRadius();
        List<Account> accs =  [SELECT ID, Name, BillingCity, BillingState, BillingPostalCode, BillingStreet, KS_Segmento__c,
                               BillingLatitude, BillingLongitude, KS_Location__Latitude__s, KS_Location__Longitude__s FROM Account
                               WHERE ( (BillingLatitude != NULL AND BillingLongitude != NULL) or (KS_Location__Latitude__s != NULL AND KS_Location__Longitude__s != NULL) )
                               AND 
                               (
                                   ((BillingLatitude <= :(userLat+maxRadius) AND BillingLatitude >= :(userLat-maxRadius))
                                   AND (BillingLongitude <= :(userLon+maxRadius) AND BillingLongitude >= :(userLon-maxRadius)))
                                   OR
                                   ((KS_Location__Latitude__s <= :(userLat+maxRadius) AND KS_Location__Latitude__s >= :(userLat-maxRadius))
                                   AND (KS_Location__Longitude__s <= :(userLon+maxRadius) AND KS_Location__Longitude__s >= :(userLon-maxRadius)))
                               )
                               ORDER BY Name ASC LIMIT 50];
        System.debug('-- JCAR -- searchCuentas -- -- - -- -- -- - ' + accs.size() + ' - CUENTAS - ' + accs);
        return accs;
    }
    @AuraEnabled
    public static List<Account> nearCuentas(Decimal userLat, Decimal userLon) {
        //if (true) { nearCuentasOLD(userLat,userLon); }
        
        System.debug('-- JCAR -- nearCuentas -- -- - -- -- -- - userLatitude - ' + userLat);
        System.debug('-- JCAR -- nearCuentas -- -- - -- -- -- - userLongitude - ' + userLon);
        
        Decimal maxRadius = KS_Account_GeoLoc_Controller.maxRadius();
        
        List<Account> accs = Database.query(
            ' SELECT ID, Name, BillingCity, BillingState, BillingPostalCode, BillingStreet, KS_Segmento__c, '
            + ' BillingLatitude, BillingLongitude, KS_Location__Latitude__s, KS_Location__Longitude__s FROM Account '
            + ' WHERE ( (BillingLatitude != NULL AND BillingLongitude != NULL) or (KS_Location__Latitude__s != NULL AND KS_Location__Longitude__s != NULL) ) '
            + '  AND  '
            + '  ( '
            + '      ((BillingLatitude <= '+(userLat+maxRadius)+' AND BillingLatitude >= '+(userLat-maxRadius)+') '
            + '       AND (BillingLongitude <= '+(userLon+maxRadius)+' AND BillingLongitude >= '+(userLon-maxRadius)+')) '
            + '     OR '
            + '      ((KS_Location__Latitude__s <= '+(userLat+maxRadius)+' AND KS_Location__Latitude__s >= '+(userLat-maxRadius)+') '
            + '       AND (KS_Location__Longitude__s <= '+(userLon+maxRadius)+' AND KS_Location__Longitude__s >= '+(userLon-maxRadius)+')) '
            + '  ) '
            + '  ORDER BY  '
            + '  DISTANCE(KS_Location__c, GEOLOCATION('+userLat+','+userLon+'), \'mi\'), '
             //DISTANCE(GEOLOCATION(BillingLatitude,BillingLongitude), GEOLOCATION(:userLat,:userLon), 'mi'),
            + '  Name ASC  '
            + '  LIMIT 50 ');
        
        System.debug('-- JCAR -- searchCuentas -- -- - -- -- -- - ' + accs.size() + ' - CUENTAS - ' + accs);
        return accs;
    }
    
    private static Decimal maxRadius() {
        KS_CONF__c conf = [SELECT NAME, KS_Value_Numeric__c FROM KS_CONF__c where NAME = 'Map_Distance'];
        return conf != null ? (conf.KS_Value_Numeric__c/1000)/100 : 0.1; // convierto metros en km y luego a decimales para lat/lon
    }
}