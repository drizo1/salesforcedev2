public with sharing class F_Trigger_Opportunity extends KS_TriggerController {
    
    public F_Trigger_Opportunity(Boolean isAfter, Boolean isBefore, Boolean isDelete, Boolean isInsert, Boolean isUndelete, Boolean isUpdate, List<sObject> lstNewItems, Map<Id, sObject> mapNewItems, List<sObject> lstOldItems, Map<Id, sObject> mapOldItems)
    {
        super(isAfter, isBefore, isDelete, isInsert, isUndelete, isUpdate, lstNewItems, mapNewItems, lstOldItems, mapOldItems);
    }
    
    protected override void runBeforeInsert() {
        ActualizarCodFes();
        ActualizarAreaAccount();
        TestDeSubida2();
        TestDeSubida3();
    }

    private void ActualizarCodFes() {
     Decimal[] onlyvalue = new Decimal[] {};
     Decimal valuenew = 0;
     String [] recordtype = new String[] {};
     String valorrecordtype = '';
     List<Opportunity> lstAccNew = (List<Opportunity>) lstNewItems;
     List<Opportunity> lstOpp = new List<Opportunity>();

     lstOpp = [SELECT F_COD_new__c,RecordTypeId FROM Opportunity WHERE RecordType.Name = 'FES' and F_COD_new__c != null ORDER BY F_COD_new__c DESC LIMIT 1];
     if(lstOpp.size() > 0) {
            onlyvalue.add(lstOpp[0].F_COD_new__c);
            recordtype.add(lstOpp[0].RecordTypeId);
            valuenew = onlyvalue[0];
            valorrecordtype = recordtype[0];

        for(Opportunity oppi: lstAccNew){
            if (oppi.RecordTypeId == valorrecordtype){
             oppi.F_COD_new__c = valuenew+1;

             System.debug(oppi.F_COD_new__c);

             if(oppi.F_COD_new__c < 100){
                oppi.F_COD__c = '19-00'+ oppi.F_COD_new__c;
             }else if(oppi.F_COD_new__c < 999 && oppi.F_COD_new__c > 99){
                oppi.F_COD__c = '19-0'+ oppi.F_COD_new__c;
             }else{
                oppi.F_COD__c = '19-'+ oppi.F_COD_new__c;
             }

             oppi.Name = oppi.F_COD__c+'_'+oppi.Name;
            }   
        }            
     }
    }

    //Proceso para actualizar el Area y el Account automáticamente  
    private void ActualizarAreaAccount() {
    String valorpais = '';
    String[] onlyArea = new String[] {};
    String onlyAreaS = '';
    String[] onlyAcc = new String[] {};
    String onlyAccS = '';
    List<Opportunity> lstAccNew = (List<Opportunity>) lstNewItems;
    List<RelacionCountryAreaAcc__c> lstOpp = new List<RelacionCountryAreaAcc__c>();

     for(Opportunity oppi: lstAccNew){
            if (oppi.RecordTypeId == '0121v000000aQqMAAU'){
                if(oppi.Country__c != null){
                    valorpais = oppi.Country__c;
                    lstOpp = [SELECT Area__c, Cuenta__c FROM RelacionCountryAreaAcc__c WHERE Country__c = :valorpais]; 
                    if(lstOpp.size() > 0){
                        onlyArea.add(lstOpp[0].Area__c);
                        onlyAreaS = onlyArea[0];

                        onlyAcc.add(lstOpp[0].Cuenta__c);
                        onlyAccS = onlyAcc[0];

                        oppi.Area__c = onlyAreaS;
                        oppi.AccountId = onlyAccS;
                    }               
                } 
            }
        }
    }

    private void TestDeSubida2() {
     Decimal[] onlyvalue3 = new Decimal[] {};
     Decimal valuenew3 = 0;
     String [] recordtype3 = new String[] {};
     String valorrecordtype3 = '';
     List<Opportunity> lstOpp3 = new List<Opportunity>();

     lstOpp3 = [SELECT F_COD_new__c,RecordTypeId FROM Opportunity WHERE F_COD_new__c != null ORDER BY F_COD_new__c DESC LIMIT 1];
    }
    private void TestDeSubida3() {
     Decimal[] onlyvalue4 = new Decimal[] {};
     Decimal valuenew4 = 0;
     String [] recordtype4 = new String[] {};
     String valorrecordtype4 = '';
     List<Opportunity> lstOpp4 = new List<Opportunity>();

     lstOpp4 = [SELECT F_COD_new__c,RecordTypeId FROM Opportunity WHERE F_COD_new__c != null ORDER BY F_COD_new__c DESC LIMIT 1];
    }
     /*for (Opportunity opp : [SELECT F_COD_new__c,RecordTypeId FROM Opportunity WHERE F_COD_new__c != null ORDER BY F_COD_new__c DESC LIMIT 1]) {
        if(opp.size() > 0) {
            onlyvalue.add(opp.F_COD_new__c);
            recordtype.add(opp.RecordTypeId);
        }
     }*/        
}