@isTest 
private class KS_TEST_Cuenta_Trigger {

    @isTest
    private static void KS_Cuenta_Trigger() {
        KS_Identidades_Usuario__c iu = new KS_Identidades_Usuario__c();
        iu.KS_Usuario_Salesforce__c = UserInfo.getUserId();
        iu.KS_Sales_rep_code__c = '999';
        insert iu;

        KS_Identidades_Usuario__c iu2 = new KS_Identidades_Usuario__c();
        iu2.KS_Usuario_Salesforce__c = UserInfo.getUserId();
        iu2.KS_Sales_rep_code__c = '888';
        insert iu2;

        Account acc = new Account();
        acc.recordTypeID = [ select id from recordType where developername = 'KS_UK' ].Id;
        acc.Name = 'Prueba999';
        acc.Type = 'Customer';
        acc.KS_Responsable1__c = '999';
        acc.KS_Responsable2__c = '999';
        acc.KS_Responsable3__c = '999';

        insert acc;
        
        acc.KS_Responsable1__c = '888';
        acc.KS_Responsable2__c = '';
        acc.KS_Responsable3__c = null;
        update acc;
        
        KS_Cuenta_Trigger.crearAccountShare(acc.Id, UserInfo.getUserId());
    }
}