public class KS_EditCleanTables {
    
    
    public KS_CleanTables__c riv{get;set;}
    public List<KS_CleanTables__c> Tabs{get;set;}    
    public String clenid{get;set;}
    public Map<String,KS_CleanTables__c> allCodes {get;set;}
   
    public KS_EditCleanTables() {
       riv = new KS_CleanTables__c();
        
       // riv.KS_FrecuenciaClean__c = ApexPages.currentPage().getParameters().get('frecuencia');
       // riv.KS_ProgramadoClean__c = ApexPages.currentPage().getParameters().get('programado');
        String ide = ApexPages.currentPage().getParameters().get('id');
        clenid = ApexPages.currentPage().getParameters().get('clenid');
        if(clenid != null){
            System.debug('---------------------------------------Axel' + ide);
        allCodes = KS_CleanTables__c.getAll();
        Tabs = allCodes.values();
       // riv.KS_Nombre_Tabla_Clean__c = ApexPages.currentPage().getParameters().get('nombre');
        //System.debug('---------------------------------------Axel' + name);
        System.debug('---------------------------------------Axel' + allCodes);
        riv = allCodes.get(clenid);
        System.debug('---------------------------------------riv ' + allCodes);
        }
        
        
    }
    
    
    
    public PageReference cancel(){
        PageReference back = Page.KS_TablesForClean;
        return back;
    }
    
    public PageReference save(){
        if(clenid == null){
            riv.Name = riv.KS_Nombre_Tabla_Clean__c;
            riv.KS_ProgramadoClean__c = 'no';
        }
        upsert riv;
		PageReference back = Page.KS_TablesForClean;
        return back;        
    }
    
}