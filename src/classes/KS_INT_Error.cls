@RestResource(urlMapping = '/Error/*')
global class KS_INT_Error {
	@HttpPost
	global static KS_Webservice_Response post() {
		KS_Webservice_Response response = new KS_Webservice_Response();
		errMsg err = new errMsg();
		RestRequest req = RestContext.request;
		system.debug('---------- Request: ' + req);

		String jSONRequestBody = req.requestBody.toString().trim();
		system.debug('------- request.body: ' + jSONRequestBody);
		err = (errMsg) JSON.deserialize(jSONRequestBody, errMsg.class);

		KS_AppErrorLog__c errLog = new KS_AppErrorLog__c();
		errLog.KS_Comentarios__c = err.comentarios;
		Datetime fec = KS_INT_Call.setDateTimeFromString(err.fecha, err.hora);
		errLog.KS_Fecha_del_error__c = fec;
		errLog.KS_Mensaje__c = err.mensaje;
		errLog.KS_Stack_trace__c = err.stackTrace;
		errLog.KS_Origen__c = err.origen;

		List<KS_AppErrorLog__c> errList = new List<KS_AppErrorLog__c> ();
		errList.add(errLog);

		response.Data = errList;
		response.Status = 'OK';
		response.Message = '';

		try {
			List<Database.SaveResult> srList = Database.insert(errList, true);
			for (Database.SaveResult sr : srList) {
				if (!sr.isSuccess()) {
					response.Status = 'ERROR';
					for (Database.Error srErr : sr.getErrors()) {
						response.Message += '#' + srErr.getStatusCode() + ':' + srErr.getMessage() + '#';
					}
					break;
				}
			}
		} catch(Exception ex) {
			response.Status = 'ERROR';
			response.Message += '#' + ex.getMessage() + '#';
		}

		return response;

	}

	global class errMsg {
		public string fecha { get; set; }
		public string hora { get; set; }
		public string mensaje { get; set; }
		public string stackTrace { get; set; }
		public string comentarios { get; set; }
		public string origen { get; set; }
	}
}