@isTest
global class KS_ContentDocument_Utils_Test {
    static testmethod void test()
    {
        
        Account acc = new Account(Name='Test ACC');
        insert acc;
        
        Attachment att = new Attachment(
            Name = 'Test ATT',
            //ContentType = 'image/jpeg',
            ParentID = acc.ID,
            Body = Blob.valueOf('Attachment')
        );
        
        Map<Integer, Attachment> attMap = new Map<Integer, Attachment>();
        attMap.put(0, att);
        
        Test.startTest();
        
        KS_ContentDocument_Utils.setAttachmentMapToContentDocumentAndGetParents(attMap);
        KS_ContentDocument_Utils.getMapContentVersionByParentID(acc.ID);
        KS_ContentDocument_Utils.setAttachmentToContentDocument(att);
        
        Test.stopTest();
    }
}