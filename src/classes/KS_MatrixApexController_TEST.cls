@isTest 
public class KS_MatrixApexController_TEST {

    public static testMethod void voidMethod_Test() {
        KS_MatrixApexController.voidMethod();
    }
    public static testMethod void getAccountList_Test() {
        KS_MatrixApexController.getAccountList();
    }
    public static testMethod void getEngagementList_Test() {
        KS_MatrixApexController.getEngagementList('');
    }
    public static testMethod void buildPickList_Test() {
        KS_MatrixApexController.buildPickList();
    }
    public static testMethod void getTotalMatriz_Test() {
        
        Integer random = Integer.valueOf((Math.random() * 9999 ) + 1000);
        User us = new User(); us.ProfileId = UserInfo.getProfileId();
        us.FirstName = 'TEST'; us.LastName = 'THIS'; 
        us.Username = random + 'TEST@THIS.com'; us.Email = us.Username;
        us.Alias = 'teth'; us.CommunityNickname = 'TESTHIS'+random; 
        us.TimeZoneSidKey = 'Europe/Paris'; us.LocaleSidKey = 'es_ES';
        us.EmailEncodingKey = 'ISO-8859-1'; us.LanguageLocaleKey = 'es';
        insert us;

        KS_Identidades_Usuario__c idUs1 = new KS_Identidades_Usuario__c();
        idUS1.KS_Usuario_Salesforce__c = UserInfo.getUserId();
        idUS1.KS_Sales_rep_code__c = 'AAAAAA';
        insert idUS1;
        
        KS_Identidades_Usuario__c idUs2 = new KS_Identidades_Usuario__c();
        idUS2.KS_Usuario_Salesforce__c = us.ID;
        idUS2.KS_Sales_rep_code__c = 'BBBBBB';
        insert idUS2;
        
        Account acc = new Account();
        acc.Name = 'Test'; 
        acc.KS_Responsable1__c = idUS1.KS_Sales_rep_code__c;
        acc.KS_Responsable2__c = idUS2.KS_Sales_rep_code__c;
        insert acc;
        
        KS_Engagement__c eng = new KS_Engagement__c();
        eng.KS_Matrix__c = 'ATTACK';
        eng.KS_Account__c = acc.ID;
        eng.KS_Customer_Potential__c = 30;
        eng.KS_TAM12__c = 10;
        eng.KS_Business_Unit__c = 'BU1';
        insert eng;
        
        Product2 prod = new Product2();
        prod.KS_Strategical_Family__c = 'FH';
        prod.Name = 'TEST';
        insert prod;
        
        KS_Sales__c sal0 = new KS_Sales__c();
        sal0.KS_Account__c = acc.ID;
        sal0.KS_Sales_LC__c = 30; sal0.KS_Cost_LC__c = 20;
        sal0.KS_External_Id__c = 'AAAAAA';
        sal0.KS_Fecha__c = Date.newInstance((Date.today().year()), 2, 2);
        //sal0.KS_Familia_estrat_gica__c = 'FH';
        sal0.KS_Producto__c = prod.ID;
        insert sal0;
        
        KS_Sales__c sal1 = new KS_Sales__c();
        sal1.KS_Account__c = acc.ID;
        sal1.KS_Sales_LC__c = 50; sal1.KS_Cost_LC__c = 10;
        sal1.KS_External_Id__c = 'BBBBBB';
        sal1.KS_Fecha__c = Date.newInstance((Date.today().year()-1), 2, 2);
        //sal1.KS_Familia_estrat_gica__c = sal0.KS_Familia_estrat_gica__c;
        sal1.KS_Producto__c = prod.ID;
        insert sal1;
        
        KS_MatrixApexController.buildPickList();
        KS_MatrixApexController.getTotalMatriz(eng.KS_Matrix__c);
        
        KS_Matriz__c mat = KS_MatrixApexController.getTotalMatrizMethod(
            eng.KS_Matrix__c, eng.KS_Business_Unit__c, prod.KS_Strategical_Family__c, idUS1.KS_Sales_rep_code__c, false);
        System.debug('-- JCAR -- getTotalMatriz_Test ' + mat);
        
        KS_MatrixApexController.getTotalMatrizMethod(
            eng.KS_Matrix__c, eng.KS_Business_Unit__c, prod.KS_Strategical_Family__c, idUS1.KS_Sales_rep_code__c, true);
    }
    public static testMethod void getSalesQRY_Test() {
        KS_MatrixApexController.getSalesQRY(new Set<Id>(), '', '', new Set<String>());
    }
    public static testMethod void MatrizWrapper_Test() {
        KS_MatrixApexController.MatrizWrapper matrz = new KS_MatrixApexController.MatrizWrapper();
        matrz.mockValues(200,2); 
        KS_Matriz__c mat = matrz.returnMatriz();
        System.debug('-- JCAR -- MatrizWrapper_Test ' + mat);
    }
}