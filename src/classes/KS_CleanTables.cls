public class KS_CleanTables {
    
    public List<KS_CleanTables__c> Tabs{get;set;}
    public String nombreEscogido{get;set;}
    public DateTime horaEscogida{get;set;}
    public String cleanid{get;set;}
    public Map<String,KS_CleanTables__c> allCodes {get;set;}
    public KS_CleanTables(){
        
        allCodes = KS_CleanTables__c.getAll();
        Tabs = allCodes.values();
        System.debug('Axel-------------------------- ' + allCodes);
    }
    
    
    public void clean(){
        System.debug('Axel-------------------------- Clean Llamado correctamente');
        
        KS_CleanTables__c ct = allCodes.get(cleanid);
        if(ct.KS_ProgramadoClean__c == 'no'){
            if(ct.KS_FrecuenciaClean__c == 'diario'){
                System.debug('Axel-------------------------- ' + ct + ' ' + allCodes);
                String CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' * * ?';
                System.debug('Axel-------------------------- ' + ct.KS_Nombre_Tabla_Clean__c);
                KS_SCHEDULE_CleanTables clean = new KS_SCHEDULE_CleanTables(ct.KS_Nombre_Tabla_Clean__c);
                System.schedule('Schedule tabla clean ' + ct.KS_Nombre_Tabla_Clean__c, CRON_STRING,clean);
                System.debug('Axel-------------------------- ' + nombreEscogido);
                ct.KS_ProgramadoClean__c = 'si';
                update ct; 
            }
            /* if(ct.KS_FrecuenciaClean__c == 'semanal'){
String CRON_STRING;
if(ct.KS_Hour_Clean__c.format('EEEE') == 'Monday'){
CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'MON'; 
}
if(ct.KS_Hour_Clean__c.format('EEEE') == 'Tuesday'){
CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'TUE'; 
}
if(ct.KS_Hour_Clean__c.format('EEEE') == 'Wednesday'){
CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'WED'; 
}
if(ct.KS_Hour_Clean__c.format('EEEE') == 'Thursday'){
CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'THU'; 
}
if(ct.KS_Hour_Clean__c.format('EEEE') == 'Friday'){
CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'FRI'; 
}
if(ct.KS_Hour_Clean__c.format('EEEE') == 'Saturday'){
CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'SAT'; 
}
else{
CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'SUN'; 
}
System.debug('Axel-------------------------- ' + ct + ' ' + allCodes);
System.debug('Axel-------------------------- ' + ct.KS_Nombre_Tabla_Clean__c);
KS_SCHEDULE_CleanTables clean = new KS_SCHEDULE_CleanTables(ct.KS_Nombre_Tabla_Clean__c);
System.schedule('Schedule tabla clean ' + ct.KS_Nombre_Tabla_Clean__c, CRON_STRING,clean);
ct.KS_ProgramadoClean__c = 'si';
update ct;   
}*/
            if(ct.KS_FrecuenciaClean__c == 'semanal'){
                System.debug('Axel-------------------------- ' + ct.KS_Day_Clean__c);
                String CRON_STRING;
                if(ct.KS_Day_Clean__c == 'lunes'){
                    CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'MON'; 
                }
                else if(ct.KS_Day_Clean__c == 'martes'){
                    CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'TUE'; 
                }
                else if(ct.KS_Day_Clean__c == 'miercoles'){
                    CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'WED'; 
                }
                else if(ct.KS_Day_Clean__c == 'jueves'){
                    CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'THU'; 
                }
                else if(ct.KS_Day_Clean__c == 'viernes'){
                    CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'FRI'; 
                }
                else if(ct.KS_Day_Clean__c == 'sabado'){
                    CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'SAT'; 
                }
                else{
                    CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ? * ' + 'SUN'; 
                }
                System.debug('Axel-------------------------- ' + ct + ' ' + allCodes);
                System.debug('Axel-------------------------- ' + ct.KS_Nombre_Tabla_Clean__c);
                KS_SCHEDULE_CleanTables clean = new KS_SCHEDULE_CleanTables(ct.KS_Nombre_Tabla_Clean__c);
                System.schedule('Schedule tabla clean ' + ct.KS_Nombre_Tabla_Clean__c, CRON_STRING,clean);
                ct.KS_ProgramadoClean__c = 'si';
                update ct;
            }
            if(ct.KS_FrecuenciaClean__c == 'mensual'){
                System.debug('Axel-------------------------- ' + ct + ' ' + allCodes);
                String CRON_STRING = '0 ' + ct.KS_Hour_Clean__c.minute() +  ' ' + ct.KS_Hour_Clean__c.hour() + ' ' + ct.KS_Hour_Clean__c.day() +' * ?';
                System.debug('Axel-------------------------- ' + ct.KS_Nombre_Tabla_Clean__c);
                KS_SCHEDULE_CleanTables clean = new KS_SCHEDULE_CleanTables(ct.KS_Nombre_Tabla_Clean__c);
                System.schedule('Schedule tabla clean ' + ct.KS_Nombre_Tabla_Clean__c, CRON_STRING,clean);
                ct.KS_ProgramadoClean__c = 'si';
                update ct;   
            }
            
            
            
            
        }else{
            String namesche = 'Schedule tabla clean ' + ct.KS_Nombre_Tabla_Clean__c;
            Id detailId = [SELECT Id FROM CronJobDetail WHERE Name=:namesche][0].Id;
            if (detailId != null) {
                Id jobId = [SELECT Id from CronTrigger WHERE CronJobDetailId = :detailId][0].Id;
                System.abortJob(jobId);
            }
            ct.KS_ProgramadoClean__c = 'no';
            update ct; 
        }
        
        
        
    } 
    public PageReference edit(){
        KS_CleanTables__c ct = allCodes.get(cleanid);
        if (ct.KS_ProgramadoClean__c == 'si'){
        String namesche = 'Schedule tabla clean ' + ct.KS_Nombre_Tabla_Clean__c;
        Id detailId = [SELECT Id FROM CronJobDetail WHERE Name=:namesche][0].Id;
        System.debug('Axel-------------------------- ' + detailId);
        
        if (detailId != null) {
            Id jobId = [SELECT Id from CronTrigger WHERE CronJobDetailId = :detailId][0].Id;
            System.abortJob(jobId);
        }
        ct.KS_ProgramadoClean__c = 'no';
        update ct; 
            
        }
        
        
        PageReference editpag = Page.KS_Programado_vf;
        
        editpag.getParameters().put('id', ct.Id);
        editpag.getParameters().put('clenid',cleanid);
        
        return editpag;       
    }
    public PageReference deleto(){
        KS_CleanTables__c ci = allCodes.get(cleanid);
        if(ci.KS_ProgramadoClean__c == 'si'){
            String namesche = 'Schedule tabla clean ' + ci.KS_Nombre_Tabla_Clean__c;
            Id detailId = [SELECT Id FROM CronJobDetail WHERE Name=:namesche][0].Id;
            if (detailId != null) {
                Id jobId = [SELECT Id from CronTrigger WHERE CronJobDetailId = :detailId][0].Id;
                System.abortJob(jobId);
            } 
        }
        try {
            delete ci;
            
        }catch(exception exc){
            
        }
        
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        pageRef.setRedirect(true);
        return pageRef;
        
    }
    
    public PageReference add(){
        PageReference addpag = Page.KS_Programado_vf;
        return addpag;
    }
    public PageReference refresh(){
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    
    
}