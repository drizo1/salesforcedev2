public with sharing class KS_Account_GeoLoc_Manual_Controller {

    @AuraEnabled
    public static String saveNewLocation (String accID, Decimal userLat, Decimal userLon) {

        System.debug('-- JCAR -- saveNewLocation -- -- - -- -- -- - accID - ' + accID);
        System.debug('-- JCAR -- saveNewLocation -- -- - -- -- -- - userLat - ' + userLat);
        System.debug('-- JCAR -- saveNewLocation -- -- - -- -- -- - userLon - ' + userLon);
        if (accID == null) { return 'ERROR_ACC'; }
        if (userLat == null) { return 'ERROR_GEO_USER'; }
        if (userLon == null) { return 'ERROR_GEO_USER'; }
         
        Account acc = new Account(ID=accID);
        acc.KS_Location__Latitude__s = userLat;
        acc.KS_Location__Longitude__s = userLon;
        update acc;
        
        return 'VALID';
    }
}