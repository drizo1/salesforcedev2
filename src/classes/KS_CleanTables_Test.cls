@isTest 
public class KS_CleanTables_Test {
    public static testMethod void TEST_KS_CleanTables () {
	        
       /*KS_CleanTables  cls = new KS_CleanTables (
            name = '1',
            KS_FrecuenciaClean__c = 'diario',
            KS_Hour_Clean__c = DateTime.now(),
            KS_Nombre_Tabla_Clean__c = 'Tabla 6',
            KS_ProgramadoClean__c = 'no'
        );*/
        
        Map<String, KS_CleanTables__c> allCodes = new Map<String, KS_CleanTables__c>();
       // allCodes.put('Tabla2', cls);
        KS_CleanTables__c ct = new KS_CleanTables__c();
        ct.KS_FrecuenciaClean__c = 'diario';
      	ct.KS_ProgramadoClean__c = 'no';
        ct.KS_Hour_Clean__c = DateTime.now();
        ct.KS_Nombre_Tabla_Clean__c = 'Tabla 6';
        ct.Name = '6';
        insert ct;
        KS_CleanTables cls = new KS_CleanTables();
        cls.nombreEscogido = 'Tabla 6';
        cls.horaEscogida = DateTime.now();
        cls.cleanid = ct.Name;
        cls.clean();
        
        // -------------------------- semanal
        ct = new KS_CleanTables__c();
        ct.KS_FrecuenciaClean__c = 'semanal';
      	ct.KS_ProgramadoClean__c = 'no';
        ct.KS_Hour_Clean__c = DateTime.now();
        ct.KS_Nombre_Tabla_Clean__c = 'Tabla 7';
        ct.Name = '7';
        ct.KS_Day_Clean__c = 'lunes';
        insert ct;
        cls = new KS_CleanTables();
        cls.nombreEscogido = 'Tabla 7';
        cls.horaEscogida = DateTime.now();
        cls.cleanid = ct.Name;
        cls.clean();
        ct.KS_Day_Clean__c = 'martes';
        upsert ct;
        cls.clean();
         // -------------------------- martes
        KS_CleanTables__c cu = new KS_CleanTables__c();
        cu.KS_FrecuenciaClean__c = 'semanal';
      	cu.KS_ProgramadoClean__c = 'no';
        cu.KS_Hour_Clean__c = DateTime.now();
        cu.KS_Nombre_Tabla_Clean__c = 'Tabla 12';
        cu.Name = '12';
        cu.KS_Day_Clean__c = 'martes';
        insert cu;
        KS_CleanTables clu = new KS_CleanTables();
        clu.nombreEscogido = 'Tabla 12';
        clu.horaEscogida = DateTime.now();
        clu.cleanid = cu.Name;
        clu.clean();
         // -------------------------- miercoles
        cu = new KS_CleanTables__c();
        cu.KS_FrecuenciaClean__c = 'semanal';
      	cu.KS_ProgramadoClean__c = 'no';
        cu.KS_Hour_Clean__c = DateTime.now();
        cu.KS_Nombre_Tabla_Clean__c = 'Tabla 13';
        cu.Name = '13';
        cu.KS_Day_Clean__c = 'miercoles';
        insert cu;
        clu = new KS_CleanTables();
        clu.nombreEscogido = 'Tabla 13';
        clu.horaEscogida = DateTime.now();
        clu.cleanid = cu.Name;
        clu.clean();        
   // -------------------------- jueves
        cu = new KS_CleanTables__c();
        cu.KS_FrecuenciaClean__c = 'semanal';
      	cu.KS_ProgramadoClean__c = 'no';
        cu.KS_Hour_Clean__c = DateTime.now();
        cu.KS_Nombre_Tabla_Clean__c = 'Tabla 14';
        cu.Name = '14';
        cu.KS_Day_Clean__c = 'jueves';
        insert cu;
        clu = new KS_CleanTables();
        clu.nombreEscogido = 'Tabla 14';
        clu.horaEscogida = DateTime.now();
        clu.cleanid = cu.Name;
        clu.clean();   
         // -------------------------- viernes
        cu = new KS_CleanTables__c();
        cu.KS_FrecuenciaClean__c = 'semanal';
      	cu.KS_ProgramadoClean__c = 'no';
        cu.KS_Hour_Clean__c = DateTime.now();
        cu.KS_Nombre_Tabla_Clean__c = 'Tabla 15';
        cu.Name = '15';
        cu.KS_Day_Clean__c = 'viernes';
        insert cu;
        clu = new KS_CleanTables();
        clu.nombreEscogido = 'Tabla 15';
        clu.horaEscogida = DateTime.now();
        clu.cleanid = cu.Name;
        clu.clean(); 
        // -------------------------- sabado
        cu = new KS_CleanTables__c();
        cu.KS_FrecuenciaClean__c = 'semanal';
      	cu.KS_ProgramadoClean__c = 'no';
        cu.KS_Hour_Clean__c = DateTime.now();
        cu.KS_Nombre_Tabla_Clean__c = 'Tabla 16';
        cu.Name = '16';
        cu.KS_Day_Clean__c = 'sabado';
        insert cu;
        clu = new KS_CleanTables();
        clu.nombreEscogido = 'Tabla 16';
        clu.horaEscogida = DateTime.now();
        clu.cleanid = cu.Name;
        clu.clean(); 
        // -------------------------- domingo
        cu = new KS_CleanTables__c();
        cu.KS_FrecuenciaClean__c = 'semanal';
      	cu.KS_ProgramadoClean__c = 'no';
        cu.KS_Hour_Clean__c = DateTime.now();
        cu.KS_Nombre_Tabla_Clean__c = 'Tabla 17';
        cu.Name = '17';
        cu.KS_Day_Clean__c = 'domingo';
        insert cu;
        clu = new KS_CleanTables();
        clu.nombreEscogido = 'Tabla 17';
        clu.horaEscogida = DateTime.now();
        clu.cleanid = cu.Name;
        clu.clean(); 
        
        // -------------------------- mensual
        ct = new KS_CleanTables__c();
        ct.KS_FrecuenciaClean__c = 'mensual';
      	ct.KS_ProgramadoClean__c = 'no';
        ct.KS_Hour_Clean__c = DateTime.now();
        ct.KS_Nombre_Tabla_Clean__c = 'Tabla 8';
        ct.Name = '8';
        
        insert ct;
        cls = new KS_CleanTables();
        cls.nombreEscogido = 'Tabla 8';
        cls.horaEscogida = DateTime.now();
        cls.cleanid = ct.Name;
        cls.clean();
        
        
        
        PageReference pg = Page.KS_TablesForClean;
        Test.setCurrentPage(pg);
        
	
        
        cls.deleto();
        //cls.edit();
        cls.add();
        cls.refresh();
        

        

	}
}