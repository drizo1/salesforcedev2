public class KS_Account_Map_Controller {
    
    public static Account accSearch {get;set;}
    
    public KS_Account_Map_Controller() { accSearch = new Account(); }
    public KS_Account_Map_Controller(ApexPages.StandardController controller) { this(); }
    
    @AuraEnabled
    public static List<Account> searchCuentas(Decimal userLat, Decimal userLon, String provincia) {
        
        System.debug('-- JCAR -- ------------------ provincia? ' + provincia);
        System.debug('-- JCAR -- ------------------ userLat? ' + userLat);
        System.debug('-- JCAR -- ------------------ userLon? ' + userLon);

        if (provincia == null) {return new List<Account>();} 
        if (provincia.trim() == '') {return new List<Account>();}
        
        String criteria = '%'+provincia+'%';criteria = criteria.replace(' ', '%');
 
        List<Account> accs = [SELECT ID,Name FROM Account 
                where BillingState LIKE :criteria
                ORDER BY Name LIMIT 1000];
        System.debug('-- JCAR -- -- -- -- -- -- -- -- -- -- - ' + accs.size() + ' - CUENTAS - ' + accs);
        return accs;
    }
}