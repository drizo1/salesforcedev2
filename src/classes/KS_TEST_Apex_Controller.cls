@isTest 
public class KS_TEST_Apex_Controller {

    public static testMethod void ControllerTesting() {
        
        String provincia = 'Barcelona';
        Decimal gpsLat = 41.4280422;
        Decimal gpsLon = 2.1832242;
        
        Account acc = new Account();
        acc.Name = 'Test';
        acc.KS_Location__Latitude__s = gpsLat;
        acc.KS_Location__Longitude__s = gpsLon;
        acc.BillingState = provincia;
        insert acc;
        
        String telf = '62626262';
        Contact con = new Contact(); con.AccountId = acc.ID;
        con.Firstname = 'Pepito'; con.Lastname = 'Flores'; con.Phone = telf;
        insert con;
        KS_Llamadas__c l = new KS_Llamadas__c();
        l.KS_Duracion__c = 10;
        l.KS_Fecha_Llamada__c = Date.today();
        l.KS_Origen__c = UserInfo.getUserId();
        l.KS_Telefono_Destino__c = telf;
        l.KS_ID_Sandbox__c = 'AA';
        insert l;
        l.KS_Tratada__c = false; update l;
        
        KS_Alarm_and_Opportunity__c alarm = new KS_Alarm_and_Opportunity__c();
        alarm.KS_Account__c = acc.ID;
        alarm.KS_External_Id__c = 'TESTEST';
        alarm.KS_Tipo_de_entrada__c = 'Alarm';
        alarm.KS_Link_to_PDF__c = 'http://www.google.com/';
        insert alarm;
        
        KS_CONF__c conf1 = new KS_CONF__c();
        conf1.NAME = 'Map_Distance'; conf1.KS_Value_Numeric__c = 1;
        insert conf1;
        KS_CONF__c conf2 = new KS_CONF__c();
        conf2.NAME = 'CheckIn_Distance'; conf2.KS_Value_Numeric__c = 1;
        insert conf2;
        
        Attachment att = new Attachment();
        att.ParentId = alarm.ID;
        att.Name = 'ATTPrueba';
        att.Body = Blob.valueOf('TEST');
        insert att;
        Map<Integer, Attachment> attMap = new Map<Integer, Attachment>();
        attMap.put(0,att);
        
        Test.startTest();
        
        // KS_Account_GeoLoc_Controller
        KS_Account_GeoLoc_Controller.searchCuentas(provincia);
        KS_Account_GeoLoc_Controller.nearCuentas(gpsLat, gpsLon);
        
        // KS_Account_GeoLoc_Manual_Controller
        KS_Account_GeoLoc_Manual_Controller.saveNewLocation(acc.ID, gpsLat, gpsLon);
            
        // KS_Account_CheckIn_Controller
        KS_Account_CheckIn_Controller.searchAccount(acc.ID);
        KS_Account_CheckIn_Controller.checkInAccount(acc.ID, gpsLat, gpsLon);
        KS_Account_CheckIn_Controller.checkOutAccount(acc.ID);

        // KS_Account_Map_Controller
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        KS_Account_Map_Controller amc = new KS_Account_Map_Controller(sc);
        KS_Account_Map_Controller.searchCuentas(gpsLat, gpsLon, provincia);
        
        // KS_Account_AlarmOpportunity_Apex
        KS_Account_AlarmOpportunity_Apex.getOppsAlarm(acc.ID, 'Alarm');
        KS_ContentDocument_Utils.setAttachmentMapToContentDocumentAndGetParents(attMap);
        KS_Account_AlarmOpportunity_Apex.getOppsAlarmWithConDoc(acc.ID, 'Alarm', true);
        
        // KS_SendMailApexController
        KS_SendMailApexController.sendEmail('customers@kaizenstep.com', 'KS_TEST_Apex_Controller');
        
        // KS_CallCreateContactController
        KS_CallCreateContactController.getAccount(acc.ID);
        KS_CallCreateContactController.getCall(l.ID);
        KS_CallCreateContactController.saveContact(con, acc.ID);
        
        // KS_CallCreateContactController_Apex
        KS_CallCreateContactController_Apex callCreateContactApex = new KS_CallCreateContactController_Apex();
        callCreateContactApex.saveContact(con);
        
        //Lookup
        Lookup.searchDB('Account', 'ID', 'Name', 10, 'Name', 'Test');
        
        //AccountsController
        AccountsController.getAccounts();
        
        //MyContactListController
        MyContactListController.getContacts(acc.ID);
        
        //Account_Alarms
        Account_Alarms.getAlarms(acc.ID);
        
        //AlarmController
        AlarmController.getAlarms();
        
        //Datos_Matriz
        Datos_Matriz.getMatrixData(acc.ID);
        
        //Demo_CRUD_CompCtrl
        Demo_CRUD_CompCtrl.getAllRecords();
        
    	Test.stopTest();
    }
}