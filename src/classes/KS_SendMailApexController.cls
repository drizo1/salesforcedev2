public class KS_SendMailApexController {
    
	@AuraEnabled
	public static String sendEmail(String destino, String body) {

        System.debug('-- JCAR -- sendEmail -- -- - -- -- -- - destino - ' + destino);
        System.debug('-- JCAR -- sendEmail -- -- - -- -- -- - body - ' + body);
        
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        emailMessage.setSubject('Email de prueba');
        String [] toAddresses = new String[] { destino };
        emailMessage.setToAddresses(toAddresses);
        emailMessage.setPlainTextBody(body);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { emailMessage });
        
        System.debug('-- JCAR -- sendEmail -- -- - -- -- -- - DONE');
        
        return '';
	}
}