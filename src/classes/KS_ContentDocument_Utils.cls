global without sharing class KS_ContentDocument_Utils {

    // SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityID in List<KS_Opportunity_hotels__c.ID> // IDs de contentdocument
    // SELECT Id, ContentDocumentId, ContentBodyId, Title, FROM ContentVersion WHERE ContentDocumentId in List<ContentDocumentLink.ContentDocumentId> // IDs de version+contentdocument
    // URL/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=<ContentVersion.ID>&contentId=<ContentDocument.ID>
    public static Map<ID,ContentVersion> getMapContentVersionByParentID(ID parentID) {
        if (parentID == null) { return new Map<ID,ContentVersion>(); }
        Set<ID> parentIDSet = new Set<ID>(); parentIDSet.add(parentID);
        return getMapContentVersionByParentIDSet(parentIDSet);
    }
    public static Map<ID,ContentVersion> getMapContentVersionByParentIDSet(Set<ID> parentIDSet) {
        Map<ID,ContentVersion> versionMap = new Map<ID,ContentVersion>();
        if (parentIDSet == null) { return versionMap; } if (parentIDSet.isEmpty()) { return versionMap; }
        
        Map<ID,ID> contentRef = new Map<ID,ID>();
        Set<ID> cdIDSet = new Set<ID>(); for (ContentDocumentLink cdl : getContentDocumentLinkFromParentIDs(parentIDSet)) 
        { cdIDSet.add(cdl.ContentDocumentId); contentRef.put(cdl.ContentDocumentId, cdl.LinkedEntityID); }
        
        for (ContentVersion cv : [SELECT Id, ContentDocumentId, ContentBodyId, Title, VersionNumber, KS_ParentID__c FROM ContentVersion WHERE ContentDocumentId in :cdIDSet ORDER BY ContentDocumentId, VersionNumber]) {
            if (versionMap.containsKey(cv.ContentDocumentId) && versionMap.get(cv.ContentDocumentId).VersionNumber > cv.VersionNumber) { continue; }
            cv.KS_ParentID__c = contentRef.get(cv.ContentDocumentId);
            versionMap.put(cv.ContentDocumentId, cv);
        }
        return versionMap;
    }
    
    public static List<ContentDocumentLink> getContentDocumentLinkFromParentIDs(Set<ID> parentIDSet) {
        if (parentIDSet==null) {return new List<ContentDocumentLink>();} else if (parentIDSet.isEmpty()) {return new List<ContentDocumentLink>();}
        return [SELECT ContentDocumentId, LinkedEntityID FROM ContentDocumentLink WHERE LinkedEntityID in :parentIDSet];
    }
    
    public static Set<ID> setAttachmentMapToContentDocumentAndGetParents(Map<Integer, Attachment> attMap) {
        Set<ID> parentIDs = new Set<ID>();
        for(ContentDocumentLink cdLink : setAttachmentMapToContentDocument(attMap)) {
            if (cdLink.LinkedEntityId != null) { parentIDs.add(cdLink.LinkedEntityId); }
        }
        return parentIDs;
    }
    
    public static ContentDocumentLink setAttachmentToContentDocument(Attachment att) {
        
        ContentVersion cv = new ContentVersion();
        cv.PathOnClient = att.Name;
        cv.VersionData = att.Body;
        cv.title = att.Name;
        insert cv;
        cv = [SELECT ID,ContentDocumentId FROM ContentVersion WHERE ID = :cv.ID];
        
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = cv.ContentDocumentId;
        cDe.LinkedEntityId = att.parentID;
        cDe.ShareType = 'V';
        cDe.Visibility = 'AllUsers';

        insert cDe;
        return cDe;
    }
    
    public static List<ContentDocumentLink> setAttachmentMapToContentDocument(Map<Integer, Attachment> attMap) {
        
        Map<Integer,ContentVersion> cvMap = new Map<Integer,ContentVersion>();
        for (Integer index : attMap.keySet()) {
            Attachment att = attMap.get(index);
            //Not that by simply inserting into ContentVersion that the actual ContentDocument entry will be created for you
            ContentVersion cv = new ContentVersion();
            cv.PathOnClient = att.Name+'.jpg'; // The files name, extension is very important here which will help the file in preview.
            cv.VersionData = att.Body;  //This is our blob
            cv.title = att.Name;
            cvMap.put(index, cv);
        }
        insert cvMap.values();
        
        
        Map<ID,Integer> cvSet = new Map<ID,Integer>();
        for (Integer index : cvMap.keySet()) { 
            cvSet.put(cvMap.get(index).ID,index);
        }        
        
        //List<ContentDocument> docus = new List<ContentDocument>();
        List<ContentDocumentLink> cdList = new List<ContentDocumentLink>();
        for (ContentVersion cv : [SELECT ID,ContentDocumentId FROM ContentVersion WHERE ID in :cvSet.keySet()]) {
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = cv.ContentDocumentId;
            cDe.LinkedEntityId = attMap.get( cvSet.get(cv.ID) ).parentID;
            cDe.ShareType = 'V';
            cDe.Visibility = 'AllUsers';
            cdList.add(cDe);
            //docus.add(new ContentDocument(id=cv.ContentDocumentId,ParentId=attMap.get( cvSet.get(cv.ID) ).parentID));
        }
        insert cdList;
        //update docus;
        return cdList;
    }
}