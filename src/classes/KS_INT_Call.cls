@RestResource(urlMapping='/Call/*')
global class KS_INT_Call {

    @HttpPost
    global static KS_Webservice_Response post()
    {
        Llamadas l = new Llamadas();
        KS_Webservice_Response response = new KS_Webservice_Response();
        
        RestRequest req = RestContext.request;
        system.debug('---------- Request: ' + req);
        
        String jSONRequestBody=req.requestBody.toString().trim();  
        system.debug('------- request.body: ' + jSONRequestBody);
        l = (Llamadas)JSON.deserialize(jSONRequestBody,Llamadas.class);
        response = setKSLlamadasFrom(l.llamadas);
        return response;
    }
    
    global static KS_Webservice_Response setKSLlamadasFrom(List<Llamada> llamadas) {
        
        KS_Webservice_Response response = new KS_Webservice_Response();
        List<KS_Llamadas__c> ksLlamadas = new List<KS_Llamadas__c>();
        
        for (Llamada l : llamadas) {
            KS_Llamadas__c ksl = new KS_Llamadas__c();
            ksl.KS_Origen__c = l.origen;
            ksl.KS_Telefono_Destino__c = l.destino;
            ksl.KS_Inicio_llamada__c = setDateTimeFromString(l.inicioFecha, l.inicioHora);
            ksl.KS_Duracion__c = Double.valueOf(l.duracion);
            ksl.KS_Tipo_de_llamada__c = l.tipo;
            ksl.KS_Identificador_llamada__c = l.identificador;
            ksLlamadas.add(ksl);
        }

        response.data = ksLlamadas;
        response.Status = 'OK';
        response.Message = '';
        try {
            List<Database.SaveResult> srList = Database.insert(ksLlamadas, true);
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) { 
                    response.Status = 'ERROR';
                    for(Database.Error err : sr.getErrors()) {
                        response.Message += '#' + err.getStatusCode() + ':' + err.getMessage() + '#';
                    }
                    break;
                }
            }
        } catch(Exception ex) {
            response.Status = 'ERROR';
            response.Message += '#' + ex.getMessage() + '#';
        }
        
        return response;
    }

	/* FORMATO:
	 * FECHA = YYYY-MM-DD
	 * HORA  = HH:MM:SS */
    global static Datetime setDateTimeFromString(String fecha, String hora) {
        Datetime dt = Datetime.newInstance(
            Integer.valueOf(fecha.split('-')[0]), Integer.valueOf(fecha.split('-')[1]), Integer.valueOf(fecha.split('-')[2]), 
            Integer.valueOf(hora.split(':')[0]), Integer.valueOf(hora.split(':')[1]), Integer.valueOf(hora.split(':')[2])
        );
        return dt;
    }
    
    global class Llamadas
    {
        public List<Llamada> llamadas { get; set; }
    }
    global class Llamada 
    {
        public String origen { get; set; }
        public String destino { get; set; }
        public String inicioFecha { get; set; }
        public String inicioHora { get; set; }
        public String duracion { get; set; }
        public String tipo { get; set; }
        public String identificador { get; set; }
    }
}