public with sharing class KS_Cuenta_Trigger extends KS_TriggerController {
    
    public KS_Cuenta_Trigger(Boolean isAfter, Boolean isBefore, Boolean isDelete, Boolean isInsert, Boolean isUndelete, Boolean isUpdate, List<sObject> lstNewItems, Map<Id, sObject> mapNewItems, List<sObject> lstOldItems, Map<Id, sObject> mapOldItems)
    {
        super(isAfter, isBefore, isDelete, isInsert, isUndelete, isUpdate, lstNewItems, mapNewItems, lstOldItems, mapOldItems);
    }
    
    protected override void runBeforeInsert() {
        //actualizarReglasColaboracion();
    }
    
    protected override void runAfterInsert() {
        actualizarReglasColaboracion();
        setCustomGeolocation(); // JCAR 28-05-2018
    }
    
    protected override void runBeforeUpdate() {
        actualizarReglasColaboracion();
    }
    
    protected override void runAfterUpdate() {
        setCustomGeolocation(); // JCAR 28-05-2018
    }
    

    private void actualizarReglasColaboracion() {
        //1) Un objecto nuevo llamado "Identidades Usuarios", que contendra un campo "KS_Sales_rep_code__c" (IDRegistroLoginFluidra) y un "User" de SF
        //2) El objecto cuentas tendra 3 campos "KS_Responsable1__c", "KS_Responsable2__c", "KS_Responsable3__c" (IDRegistroLoginFluidra)
        //3) Trigger cuentas para creacion / modificacion - Llamara al objecto "Identidades Usuarios" y compartira la cuenta con los usuarios que coincidan con los "IDRegistroLoginFluidra"
        //4) Cargaremos una lista de cuentas con los datos de los 3 campo "IDRegistroLoginFluidra", que al guardarse actualizaran sus reglas de colaboracion
        
        List<Account> lstAccNew = (List<Account>) lstNewItems;
        Map<Id, Account> mapAccOld = isUpdate ? (Map<Id, Account>) mapOldItems : new Map<Id, Account>();
        Set<String> setIdResponsable = new Set<String>();
        Set<String> setIdResponsableActual = new Set<String>();
        
        Map<String,Set<String>> respINSERT = new Map<String,Set<String>>();
        Map<String,Set<String>> respDELETE = new Map<String,Set<String>>();
        
        //Listamos los responsables
        for(Account acc : lstAccNew) {
            
            Account accOld = isUpdate ? mapAccOld.get(acc.Id) : new Account();
            
            if (isUpdate) {
                if (accOld.KS_Responsable1__c == acc.KS_Responsable1__c
                    && accOld.KS_Responsable2__c == acc.KS_Responsable2__c
                    && accOld.KS_Responsable3__c == acc.KS_Responsable3__c) 
                {continue;}
            }
            
            Set<String> respINSERTset = isUpdate&&respINSERT.containsKey(acc.ID) ? respINSERT.get(acc.ID) : new Set<String>();
            Set<String> respDELETEset = isUpdate&&respDELETE.containsKey(acc.ID) ? respDELETE.get(acc.ID) : new Set<String>();
            
            if(acc.KS_Responsable1__c!=null && acc.KS_Responsable1__c!='' && !setIdResponsable.contains(acc.KS_Responsable1__c)) {
                if (isInsert || (isUpdate&&acc.KS_Responsable1__c!=accOld.KS_Responsable1__c) ) {
                    setIdResponsableActual.add(acc.KS_Responsable1__c); 
                    respINSERTset.add(acc.KS_Responsable1__c);
                }
            }
            if(acc.KS_Responsable2__c!=null && acc.KS_Responsable2__c!='' && !setIdResponsable.contains(acc.KS_Responsable2__c)) {
                if (isInsert || (isUpdate&&acc.KS_Responsable2__c!=accOld.KS_Responsable2__c) ) {
                    setIdResponsableActual.add(acc.KS_Responsable2__c); 
                    respINSERTset.add(acc.KS_Responsable2__c);
                }
            }
            if(acc.KS_Responsable3__c!=null && acc.KS_Responsable3__c!='' && !setIdResponsable.contains(acc.KS_Responsable3__c)) {
                if (isInsert || (isUpdate&&acc.KS_Responsable3__c!=accOld.KS_Responsable3__c) ) {
                    setIdResponsableActual.add(acc.KS_Responsable3__c); 
                    respINSERTset.add(acc.KS_Responsable3__c);
                }
            }
            
            //Si es Update - Listamos los responsables anteriores 
            if(isUpdate) {
                
                if(accOld.KS_Responsable1__c!=null && accOld.KS_Responsable1__c!='') {
                    Boolean borrado = acc.KS_Responsable1__c==null || (acc.KS_Responsable1__c!=null && acc.KS_Responsable1__c.trim()!='');
                    if(borrado) { respDELETEset.add(accOld.KS_Responsable1__c); setIdResponsable.add(accOld.KS_Responsable1__c); }
                }
                if(accOld.KS_Responsable2__c!=null && accOld.KS_Responsable2__c!='') {
                    Boolean borrado = acc.KS_Responsable2__c==null || (acc.KS_Responsable2__c!=null && acc.KS_Responsable2__c.trim()!='');
                    if(borrado) { respDELETEset.add(accOld.KS_Responsable2__c); setIdResponsable.add(accOld.KS_Responsable2__c); }
                }
                if(accOld.KS_Responsable3__c!=null && accOld.KS_Responsable3__c!='') {
                    Boolean borrado = acc.KS_Responsable3__c==null || (acc.KS_Responsable3__c!=null && acc.KS_Responsable3__c.trim()!='');
                    if(borrado) { respDELETEset.add(accOld.KS_Responsable3__c); setIdResponsable.add(accOld.KS_Responsable3__c);  }
                }
                
                for (String repK : respINSERTset) {
                    respDELETEset.remove(repK);
                }
            }
            
            respINSERT.put(acc.ID, respINSERTset);
            respDELETE.put(acc.ID, respDELETEset);
        }
        
        if(setIdResponsableActual.size()>0) setIdResponsable.addAll(setIdResponsableActual);
        
        //Encontramos el User correspondiente a la lista de responsables
        Map<String, Id> mapIdentUser = new Map<String, Id>();
        for(KS_Identidades_Usuario__c iu : [ select id, KS_Usuario_Salesforce__c, KS_Sales_rep_code__c from KS_Identidades_Usuario__c where KS_Sales_rep_code__c in :setIdResponsable ]) {
            if(!mapIdentUser.containsKey(iu.KS_Sales_rep_code__c)) {
                mapIdentUser.put(iu.KS_Sales_rep_code__c, iu.KS_Usuario_Salesforce__c);
            }
        }
        
        //Crear Colaboracion o Eliminar Colaboracion de las Cuentas con los Usuarios correspondientes por los responsables informados en sus correspondiente campos 
        List<AccountShare> lstAccShareNew = new List<AccountShare>();
        Set<Id> setIdAccDelShare = new Set<Id>();
        Set<Id> setIdUserDelShare = new Set<Id>();
        
        /*for(Account acc : lstAccNew) {
            //Insert - Crear Colaboracion
            if(isInsert) {
                if(acc.KS_Responsable1__c!=null && acc.KS_Responsable1__c!='' && mapIdentUser.containsKey(acc.KS_Responsable1__c) && mapIdentUser.get(acc.KS_Responsable1__c)!=acc.OwnerId) 
                    lstAccShareNew.add(crearAccountShare(acc.Id, mapIdentUser.get(acc.KS_Responsable1__c)));
                if(acc.KS_Responsable2__c!=null && acc.KS_Responsable2__c!='' && mapIdentUser.containsKey(acc.KS_Responsable2__c) && mapIdentUser.get(acc.KS_Responsable2__c)!=acc.OwnerId) 
                    lstAccShareNew.add(crearAccountShare(acc.Id, mapIdentUser.get(acc.KS_Responsable2__c)));
                if(acc.KS_Responsable3__c!=null && acc.KS_Responsable3__c!='' && mapIdentUser.containsKey(acc.KS_Responsable3__c) && mapIdentUser.get(acc.KS_Responsable3__c)!=acc.OwnerId) 
                    lstAccShareNew.add(crearAccountShare(acc.Id, mapIdentUser.get(acc.KS_Responsable3__c)));
            }
            
            //Update - Crear y Eliminar Colaboracion
            if(isUpdate) {
                System.debug('----------------------- SERG - mapAccOld.get(acc.Id).KS_Responsable1__c: ' + mapAccOld.get(acc.Id).KS_Responsable1__c);
                System.debug('----------------------- SERG - mapAccOld.get(acc.Id).KS_Responsable1__c: ' + mapAccOld.get(acc.Id).KS_Responsable2__c);
                System.debug('----------------------- SERG - mapAccOld.get(acc.Id).KS_Responsable1__c: ' + mapAccOld.get(acc.Id).KS_Responsable3__c);
                
                if(acc.KS_Responsable1__c!=mapAccOld.get(acc.Id).KS_Responsable1__c) {
                    if(acc.KS_Responsable1__c!=null && acc.KS_Responsable1__c!='' && mapIdentUser.containsKey(acc.KS_Responsable1__c) && mapIdentUser.get(acc.KS_Responsable1__c)!=acc.OwnerId) 
                        lstAccShareNew.add(crearAccountShare(acc.Id, mapIdentUser.get(acc.KS_Responsable1__c)));
                    if(!setIdAccDelShare.contains(acc.Id)) 
                        setIdAccDelShare.add(acc.Id);
                    if(mapAccOld.get(acc.Id).KS_Responsable1__c!=null && mapAccOld.get(acc.Id).KS_Responsable1__c!='' && !setIdUserDelShare.contains(mapIdentUser.get(mapAccOld.get(acc.Id).KS_Responsable1__c)) && !setIdResponsableActual.contains(mapAccOld.get(acc.Id).KS_Responsable1__c)) 
                        setIdUserDelShare.add(mapIdentUser.get(mapAccOld.get(acc.Id).KS_Responsable1__c));
                } 
                
                if(acc.KS_Responsable2__c!=mapAccOld.get(acc.Id).KS_Responsable2__c) {
                    if(acc.KS_Responsable1__c!=null && acc.KS_Responsable1__c!='' && mapIdentUser.containsKey(acc.KS_Responsable2__c) && mapIdentUser.get(acc.KS_Responsable2__c)!=acc.OwnerId) 
                        lstAccShareNew.add(crearAccountShare(acc.Id, mapIdentUser.get(acc.KS_Responsable2__c)));
                    if(!setIdAccDelShare.contains(acc.Id)) 
                        setIdAccDelShare.add(acc.Id);
                    if(mapAccOld.get(acc.Id).KS_Responsable2__c!=null && mapAccOld.get(acc.Id).KS_Responsable2__c!='' && !setIdUserDelShare.contains(mapIdentUser.get(mapAccOld.get(acc.Id).KS_Responsable2__c)) && !setIdResponsableActual.contains(mapAccOld.get(acc.Id).KS_Responsable2__c)) 
                        setIdUserDelShare.add(mapIdentUser.get(mapAccOld.get(acc.Id).KS_Responsable2__c));
                } 
                
                if(acc.KS_Responsable3__c!=mapAccOld.get(acc.Id).KS_Responsable3__c) {
                    if(acc.KS_Responsable3__c!=null && acc.KS_Responsable3__c!='' && mapIdentUser.containsKey(acc.KS_Responsable3__c) && mapIdentUser.get(acc.KS_Responsable3__c)!=acc.OwnerId) 
                        lstAccShareNew.add(crearAccountShare(acc.Id, mapIdentUser.get(acc.KS_Responsable3__c)));
                    if(!setIdAccDelShare.contains(acc.Id)) 
                        setIdAccDelShare.add(acc.Id);
                    if(mapAccOld.get(acc.Id).KS_Responsable3__c!=null && mapAccOld.get(acc.Id).KS_Responsable3__c!='' && !setIdUserDelShare.contains(mapIdentUser.get(mapAccOld.get(acc.Id).KS_Responsable3__c)) && !setIdResponsableActual.contains(mapAccOld.get(acc.Id).KS_Responsable3__c)) 
                        setIdUserDelShare.add(mapIdentUser.get(mapAccOld.get(acc.Id).KS_Responsable3__c));
                }   
            }
        }*/
        
        System.debug('----------------------- JCAR - respINSERT: ' + respINSERT);
        System.debug('----------------------- JCAR - respDELETE: ' + respDELETE);
        
        for (String acc : respINSERT.keySet()) {
            ID owner = ((Account)mapNewItems.get(acc)).OwnerID;
            for (String resp : respINSERT.get(acc)) {
                if (mapIdentUser.containsKey(resp) && mapIdentUser.get(resp) != owner) {
                    lstAccShareNew.add(crearAccountShare(acc, mapIdentUser.get(resp)));
                }
            }
        }
        for (String acc : respDELETE.keySet()) {
            if(respDELETE.get(acc) != null && !respDELETE.get(acc).isEmpty()) {
                setIdAccDelShare.add(acc);
                for (String resp : respDELETE.get(acc)) {
                    if (mapIdentUser.containsKey(resp)) {
                        setIdUserDelShare.add(mapIdentUser.get(resp));
                    }
                }
            }
        }
        
        System.debug('----------------------- SERG - lstAccShareNew: ' + lstAccShareNew);

        if(lstAccShareNew.size()>0) { insert lstAccShareNew; }
        
        System.debug('----------------------- SERG - setIdAccDelShare: ' + setIdAccDelShare);
        System.debug('----------------------- SERG - setIdUserDelShare: ' + setIdUserDelShare);
        
        if(setIdAccDelShare.size()>0 && setIdUserDelShare.size()>0) {
            List<AccountShare> lstAccShareDel = new List<AccountShare>();
            for (AccountShare accSh : [select id,AccountId,UserOrGroupId 
                                       from AccountShare where AccountId in :setIdAccDelShare and UserOrGroupId in :setIdUserDelShare and rowCause!='Owner' ]) 
            {
                if (respDELETE.containsKey(accSh.AccountId)) {
                    for (String resp : respDELETE.get(accSh.AccountId)) {
                        if (mapIdentUser.get(resp) == accSh.UserOrGroupId) { lstAccShareDel.add(accSh); }
                    }
                }
            }
            System.debug('----------------------- SERG - lstAccShareDel: ' + lstAccShareDel);
            if(lstAccShareDel.size()>0) { delete lstAccShareDel; }
        }
    }
    
    public static AccountShare crearAccountShare(Id idAcc, String idUser) {
        AccountShare accShare = new AccountShare();
        accShare.accountId = idAcc;
        accShare.userOrGroupId = idUser;
        accShare.accountAccessLevel = 'Edit'; //Read or Edit;
        accShare.opportunityAccessLevel = 'Edit';
        accShare.caseAccessLevel = 'Edit';
        //accShare.ContactAccessLevel = 'Edit';
        return accShare;
    }
    
    public static Boolean customGeoLaunched = false;
    public void setCustomGeolocation() {
        if (customGeoLaunched) {return;}
        List<Account> accUpdate = new List<Account>();
        for (Account acc : (List<Account>) lstNewItems) {
            
            if (acc.ID != null 
                && acc.KS_Location__Latitude__s == null
                && acc.KS_Location__Longitude__s == null) {
                    
                    if (acc.BillingLatitude != null && acc.BillingLongitude != null) {
                        
                        Account cuenta = new Account(ID=acc.ID);
                        cuenta.KS_Location__Latitude__s = acc.BillingLatitude;
                        cuenta.KS_Location__Longitude__s = acc.BillingLongitude;
                        accUpdate.add(cuenta);
                    }
                }
        }
        customGeoLaunched = !accUpdate.isEmpty();
        if (!accUpdate.isEmpty()) {update accUpdate;}
    }
}