({
    doInit : function(component, event, helper) {
        var action = component.get("c.getTotalMatrizFiltrado");
        action.setParams({ 
            matrix : component.get("v.matrixType"),
            matrixBSU : event.getParam("matrixBSU"),
            matrixFAM : event.getParam("matrixFAM"),
            matrixUSR : event.getParam("matrixUSR")
        });
        action.setCallback(this, function(a) {
            component.set("v.matrixValues", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    doSearch : function(component, event, helper) {
        var action = component.get("c.getTotalMatrizFiltrado");
        
        var matrixBSUvalue = event.getParam("matrixBSU");
        if (matrixBSUvalue == 'null' || matrixBSUvalue == '' || matrixBSUvalue == undefined) 
        { matrixBSUvalue = component.get("v.matrixBSU"); }
        
        var matrixFAMvalue = event.getParam("matrixFAM");
        if (matrixFAMvalue == 'null' || matrixFAMvalue == '' || matrixFAMvalue == undefined) 
        { matrixFAMvalue = component.get("v.matrixFAM"); }
        
        var matrixUSRvalue = event.getParam("matrixUSR");
        if (matrixUSRvalue == 'null' || matrixUSRvalue == '' || matrixUSRvalue == undefined) 
        { matrixUSRvalue = component.get("v.matrixUSR"); }
        
        action.setParams({ 
            matrix : component.get("v.matrixType"),
            matrixBSU : matrixBSUvalue,
            matrixFAM : matrixFAMvalue,
            matrixUSR : matrixUSRvalue
        });
        action.setCallback(this, function(a) {
            component.set("v.matrixValues", a.getReturnValue());
            if (matrixBSUvalue != 'null' && matrixBSUvalue != '' && matrixBSUvalue != undefined) 
            { component.set("v.matrixBSU", matrixBSUvalue); }
            if (matrixFAMvalue != 'null' && matrixFAMvalue != '' && matrixFAMvalue != undefined) 
            { component.set("v.matrixFAM", matrixFAMvalue); }
            if (matrixUSRvalue != 'null' && matrixUSRvalue != '' && matrixUSRvalue != undefined) 
            { component.set("v.matrixUSR", matrixUSRvalue); }
        });
        $A.enqueueAction(action);
    }
})