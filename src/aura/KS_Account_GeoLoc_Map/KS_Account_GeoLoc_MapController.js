({
    jsLoaded: function(component, event, helper) {
        var map = L.map('map', {zoomControl: true}).setView([0, 0], 16);
        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
            {
                attribution: 'Tiles © Esri'
            }).addTo(map);
        component.set("v.map", map);
    },
 
    accountsLoaded: function(component, event, helper) {
 
        var accounts = event.getParam('accounts');
        if (accounts == undefined) {alert('Accounts not found.');return;}
        if (accounts[0] == undefined) {alert('Accounts not found.');return;}
        
        var map = component.get('v.map'); map.remove();
        // Add markers
        if (accounts[0].KS_Location__Latitude__s != undefined && accounts[0].KS_Location__Longitude__s != undefined) {
            map = L.map('map', {zoomControl: true}).setView([accounts[0].KS_Location__Latitude__s, accounts[0].KS_Location__Longitude__s], 12);
        } else {
            map = L.map('map', {zoomControl: true}).setView([accounts[0].BillingLatitude, accounts[0].BillingLongitude], 12);
        }
        /*L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
            {
                attribution: 'Tiles © Esri'
            }).addTo(map);*/
        
        var osmAttr = '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    {
                        attribution: osmAttr
                    }).addTo(map);
        
        component.set("v.map", map);
               
        for (var i=0; i<accounts.length; i++) {
            var account = accounts[i]; var latLng = null;
            
            if (account.KS_Location__Latitude__s != undefined && account.KS_Location__Longitude__s != undefined) {
                latLng = [account.KS_Location__Latitude__s, account.KS_Location__Longitude__s];
            } else if (account.BillingLatitude != undefined && account.BillingLongitude != undefined) {
                latLng = [account.BillingLatitude, account.BillingLongitude];
            }
            
            if (latLng != null) {

                var mark_icon = null; var iconName = '/marker_unk.png';
                if (account.KS_Segmento__c != undefined) {
                    if (account.KS_Segmento__c == 'A') { iconName = '/marker_A.png'; }
                    if (account.KS_Segmento__c == 'B') { iconName = '/marker_B.png'; }
                    if (account.KS_Segmento__c == 'C') { iconName = '/marker_C.png'; }
                    if (account.KS_Segmento__c == 'V') { iconName = '/marker_V.png'; } 
                }
                mark_icon = L.icon({iconUrl: $A.get('$Resource.map_marker') + iconName});
                L.marker(latLng, {account: account, icon: mark_icon}).addTo(map).on('click', function(event) {
                    helper.navigateToDetailsView(event.target.options.account.Id);
                });
            }
        } 
    },
 
    accountSelected: function(component, event, helper) {
        // Center the map on the account selected in the list
        var map = component.get('v.map');
        var account = event.getParam("account");
        
        if (account.KS_Location__Latitude__s != undefined && account.KS_Location__Longitude__s != undefined) {
            map.panTo([account.KS_Location__Latitude__s, account.KS_Location__Longitude__s]);    
        } else {
            map.panTo([account.BillingLatitude, account.BillingLongitude]);    
        }
    }

})