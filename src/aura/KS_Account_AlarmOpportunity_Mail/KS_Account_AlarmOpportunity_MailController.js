({
    doOpenSendMail: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", event.getParam('isOpen'));
        component.set("v.attachedURL", event.getParam('attachedURL'));
    },
    
    doCloseSendMail: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    doSendMail: function(component, event, helper) {
        var sendAction = component.get("c.sendEmail");
        sendAction.setParams({
            "destino": component.get("v.sendToMail"),
            "body": component.get("v.bodyToMail") + "\r\n" + component.get("v.attachedURL")
        });
        sendAction.setCallback(this, function(data) {
            var event = $A.get("e.c:KS_Account_AlarmOpportunity_MailShow");
            event.setParams({"attachedURL": "", "isOpen": false});
            event.fire();
        });
        $A.enqueueAction(sendAction);
    },
})