({
    doInit: function(component, event, helper) {

        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        if (!navigator.geolocation){ alert('Map: Geolocation is not supported by your browser'); return; }
        
        function success(position) {
            
            var action = component.get("c.nearCuentas");
            //var latitude  = position.coords.latitude;
            //var longitude = position.coords.longitude;
            action.setParams({ 
                userLat : position.coords.latitude,
                userLon : position.coords.longitude
            });
            action.setCallback(this, function(a) {
                component.set("v.accounts", a.getReturnValue());
                var event = $A.get("e.c:KS_Account_GeoLoc_Loaded");
                event.setParams({"accounts": a.getReturnValue()});
                event.fire();
            });
            $A.enqueueAction(action);
        }
        
        function error() { alert('Map: Unable to retrieve your location'); }
        navigator.geolocation.getCurrentPosition(success, error, options);
    },
    doSearch : function(component, event) {
        var action = component.get("c.searchCuentas");
        action.setParams({ provincia : event.getParam('provincia') });
        action.setCallback(this, function(a) {
            component.set("v.accounts", a.getReturnValue());
            var event = $A.get("e.c:KS_Account_GeoLoc_Loaded");
                event.setParams({"accounts": a.getReturnValue()});
            event.fire();
        });
        $A.enqueueAction(action);
    }
})