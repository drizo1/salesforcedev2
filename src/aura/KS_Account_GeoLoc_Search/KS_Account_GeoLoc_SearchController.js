({
    doSearch : function(component, event) {
        var action = component.get("c.nothing");
        action.setCallback(this, function(a) {
            var event = $A.get("e.c:KS_Account_GeoLoc_Searched");
            event.setParams({"provincia": component.get("v.provincia")});
            event.fire();
        });
    $A.enqueueAction(action);
    }
})