({
    doInit : function(component, event, helper) {

        component.set("v.isLoading", true);
        $A.util.removeClass(component.find("header"), 'slds-theme_error');
        $A.util.removeClass(component.find("header"), 'slds-theme_alert-texture');
        component.set("v.error_hed", 'Check In/Out');
        component.set("v.error_msg", 'Loading.');
        
        var error = false;
        var error_msg = '';
        
        var cuenta = component.get("v.recordId");
        if (cuenta == undefined || cuenta == null) { error_msg = 'Load: Account not found.'; return; }
        
        var action = component.get("c.searchAccount");
        action.setParams({ accID : cuenta });
        action.setCallback(this, function(response) {
            var event = $A.get("e.c:KS_Account_CheckIn_doIt");
            var acc = response.getReturnValue();
            if (acc == undefined || acc == null) { error_msg = 'Load: Account not found.'; event.setParams({"doIt": false}); error = true; }
            else {
                
                var isChecked = acc.KS_CheckIn__c;
                if (
                    ( 
                        (acc.BillingLatitude != undefined && acc.BillingLatitude != null)
                        &&
                        (acc.BillingLongitude != undefined && acc.BillingLongitude != null)
                    )
                    ||
                    ( 
                        (acc.KS_Location__Latitude__s != undefined && acc.KS_Location__Latitude__s != null)
                        && 
                        (acc.KS_Location__Longitude__s != undefined && acc.KS_Location__Longitude__s != null)
                    )
                ) 
                { 
                    event.setParams({"doIt": true});
                }
                else {
                    if (isChecked) { event.setParams({"doIt": true});
                    } else { error_msg = 'Load: Account geolocation not valid.'; event.setParams({"doIt": false}); error = true; }
                }
                event.setParams({"doIn": !isChecked});
            }
            event.fire();
        });
        $A.enqueueAction(action);

        var header = component.find("header");
        $A.util.removeClass(header, 'slds-theme_error');
        $A.util.removeClass(header, 'slds-theme_alert-texture');
        if(error) {
            $A.util.addClass(header, 'slds-theme_error');
            $A.util.addClass(header, 'slds-theme_alert-texture');
            component.set("v.error_msg", error_msg);
            component.set("v.isLoading", false);
        } else {
            component.set("v.error_msg", 'Load: Account Location Loaded.');
        }
        
        /*action.setCallback(this, function(a) {
            //component.set("v.accounts", a.getReturnValue());
            //var event = $A.get("e.c:KS_Account_GeoLoc_Loaded");
            //event.setParams({"accounts": a.getReturnValue()});
            //event.fire();
        });*/
        //$A.enqueueAction(action);
    },
    
    doIt : function(component, event, helper) {
        
        function showError(cmp, isError, msgError, isIn) {
            component.set("v.isLoading", false);
            var header = cmp.find("header");
            $A.util.removeClass(header, 'slds-theme_error');
            $A.util.removeClass(header, 'slds-theme_alert-texture');
            if(isError) {
                $A.util.addClass(header, 'slds-theme_error');
                $A.util.addClass(header, 'slds-theme_alert-texture');
                cmp.set("v.error_msg", msgError);
            }
            if (isIn) { cmp.set("v.error_hed", "Check In"); }
            else { cmp.set("v.error_hed", "Check Out"); cmp.set("v.error_msg", msgError); }
        }
        
        var cuenta = component.get("v.recordId");
        if (cuenta == undefined || cuenta == null) { showError(component, true,'Check In: Account not found.', true); return; }
        
        var doIt = event.getParam('doIt');
        if (!doIt) { showError(component, true,'Check In: Unable to process invalid Account.', true); return; }
        
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        if (!navigator.geolocation){ showError(component, true,'Check In: Geolocation is not supported by your browser', true); return; }
        
        /*
            var action = component.get("c.nearCuentas");
            //var latitude  = position.coords.latitude;
            //var longitude = position.coords.longitude;
            action.setParams({ 
                userLat : position.coords.latitude,
                userLon : position.coords.longitude
            });
            action.setCallback(this, function(a) {
                component.set("v.accounts", a.getReturnValue());
                var event = $A.get("e.c:KS_Account_GeoLoc_Loaded");
                event.setParams({"accounts": a.getReturnValue()});
                event.fire();
            });
            $A.enqueueAction(action);
        */
        function success(position) {
            
            //var latitude  = position.coords.latitude;
            //var longitude = position.coords.longitude;
            
            var action = component.get("c.checkInAccount");
            action.setParams({ 
                accID : component.get("v.recordId"),
                userLat : position.coords.latitude,
                userLon : position.coords.longitude 
            });
            action.setCallback(this, function(response) {
                var status = response.getReturnValue();                
                if (status != null && status.length > 1 && status[0] == 'VALID') {
                    //showError(component, true,'Check In: Check In Successful.', true);
                    component.set("v.isLoading", true);
                    component.set("v.error_msg", 'Checking...');
                    var evnt = $A.get("e.c:KS_Account_CheckIn_Create");
                    evnt.setParams({
                        "checkId" : status[1],
                        "doIt" : doIt,
                        "doIn" : true});
                    evnt.fire();
                    //$A.get("e.force:closeQuickAction").fire()
                } else {
                    if (status[0] == 'ERROR_GEO_ACC') {
                        showError(component, true,'Check In: Unable to process invalid Account geolocation data.', true);
                    } else if (status[0] == 'ERROR_GEO_USER') {
                        showError(component, true,'Check In: Unable to process invalid User geolocation data.', true);
                    } else if (status[0] == 'INVALID') {
                        showError(component, true,'Check In: Unable to Check In, Account is not at reach.', true);
                    } else {
                        //alert('Check In: Unexpected Error.');
                        showError(component, true,'Check In: Unexpected Error', true);
                    }                
                }
            });
            $A.enqueueAction(action);
        }
        
        function error() { showError(component, true,'Check In: Unable to retrieve your location', true); }
        
        var doIn = event.getParam('doIn');

        if (doIn) {
            //alert("DO CHECK IN PLEASE");
            //navigator.geolocation.getCurrentPosition(success, error, options);
            component.set("v.error_msg", 'Load: Retrieving User Location.');
            navigator.geolocation.getCurrentPosition(function(geoloc) {
                //alert(geoloc.coords.latitude + ', ' + geoloc.coords.longitude);
                var evnt = component.getEvent("doCheckCall");
                evnt.setParams({
                    "accID" : component.get("v.recordId"),
                    "userLat" : geoloc.coords.latitude,
                    "userLon" : geoloc.coords.longitude 
                });
                evnt.fire();
            }, function() {
                //alert('There was an error.');
                showError(component, true,'Check In: Unable to retrieve your location', true);
            });
            //alert("something happened?");
        } else {
            var action = component.get("c.checkOutAccount");
            action.setParams({ 
                accID : component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                var status = response.getReturnValue();
                if (status != null && status[0] == 'CHECKOUT_OK') {
                    //showError(component, false,'Check Out: Successful.', false);
                    var evnt = $A.get("e.c:KS_Account_CheckIn_Create");
                    evnt.setParams({
                        "checkId" : status[1],
                        "doIt" : doIt,
                        "doIn" : false
                    });
                    evnt.fire();
                    //$A.get("e.force:closeQuickAction").fire()
                } else if (status != null && status[0] == 'CHECKOUT_KO') {
                    showError(component, true,'Check Out: Unable to Check Out', false);
                } else {
                    showError(component, true,'Check Out: Unexpected Error', false);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    doCheckIn : function(component, event, helper) {
        
        function showError(cmp, isError, msgError, isIn) {
            component.set("v.isLoading", false);
            var header = cmp.find("header");
            $A.util.removeClass(header, 'slds-theme_error');
            $A.util.removeClass(header, 'slds-theme_alert-texture');
            if(isError) {
                $A.util.addClass(header, 'slds-theme_error');
                $A.util.addClass(header, 'slds-theme_alert-texture');
                cmp.set("v.error_msg", msgError);
            }
            if (isIn) { cmp.set("v.error_hed", "Check In"); }
            else { cmp.set("v.error_hed", "Check Out"); cmp.set("v.error_msg", msgError); }
        }
        
        var action = component.get("c.checkInAccount");
        action.setParams({ 
            accID : event.getParam("accID"),//component.get("v.recordId"),
            userLat : event.getParam("userLat"),//position.coords.latitude,
            userLon : event.getParam("userLon")//position.coords.longitude 
        });
        action.setCallback(this, function(response) {
            var status = response.getReturnValue();                
            if (status != null && status.length > 1 && status[0] == 'VALID') {
                //showError(component, true,'Check In: Check In Successful.', true);
                var evnt = $A.get("e.c:KS_Account_CheckIn_Create");
                component.set("v.isLoading", true);
                component.set("v.error_msg", 'Checking...');
                evnt.setParams({
                    "checkId" : status[1],
                    "doIt" : true,
                    "doIn" : true
                });
                evnt.fire();
                //$A.get("e.force:closeQuickAction").fire()
            } else {
                if (status[0] == 'ERROR_GEO_ACC') {
                    showError(component, true,'Check In: Unable to process invalid Account geolocation data.', true);
                } else if (status[0] == 'ERROR_GEO_USER') {
                    showError(component, true,'Check In: Unable to process invalid User geolocation data.', true);
                } else if (status[0] == 'INVALID') {
                    showError(component, true,'Check In: Unable to Check In, Account is not at reach.', true);
                } else {
                    //alert('Check In: Unexpected Error.');
                    showError(component, true,'Check In: Unexpected Error', true);
                }                
            }
        });
        $A.enqueueAction(action);
        //alert(action);
    },
    
    doCreate : function(component, event, helper) {

        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": event.getParam("checkId"),
            "entityApiName": "KS_CheckIn__c",
            "defaultFieldValues": {
                'KS_Account__c' : component.get("v.recordId"),
                fieldDateTime : new Date()
            }
        });
        editRecordEvent.fire();
    },
    
    doOpenCheck : function(component, event, helper) {

        var doIn = event.getParam("doIn");
        component.set("v.isLoading", false);
        component.set("v.error_msg", '');
        component.set("v.error_hed", doIn?'Check In':'Check Out');
        
        //if (doIn) {
            component.set("v.checkId", event.getParam("checkId"));
            component.set("v.editCheck", true);
            return;
        //}
        /*
        component.set("v.error_msg", 'Checking succesful.');
        
        var navigate = $A.get(doIn?"e.force:editRecord":"e.force:navigateToSObject");
        //if (doIn) { navigate = $A.get("e.force:editRecord"); }
        if (navigate != undefined) {
            navigate.setParams({
                "recordId": event.getParam("checkId")
            });
            navigate.fire();
        } else {
            location.href = '/'+event.getParam("checkId")+(doIn?'/e':'');
        }
        */
    },
    
    backToAccount : function(component, event, helper) {
        var navigate = $A.get("e.force:navigateToSObject");
        navigate.setParams({
            "recordId": component.get("v.checkId")
        });
        navigate.fire();
    }
})