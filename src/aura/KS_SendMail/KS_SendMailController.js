({
    handleSend: function(component, event, helper) {
        var saveContactAction = component.get("c.saveContact");
        saveContactAction.setParams({
            "destino": component.get("v.email"),
            "body": component.get("v.body"),
            "parentID": component.get("v.recordId")
        });
    },
    handleCancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})