({
  // Fetch the Alarms from the Apex controller
  getAlarmList: function(component) {
    var action = component.get('c.getAlarms');

    // Set up the callback
    var self = this;
    action.setCallback(this, function(actionResult) {
     component.set('v.alarms', actionResult.getReturnValue());
    });
    $A.enqueueAction(action);
  }
})