({
    doInit : function(component, event, helper) {
        // Get a reference to the getAlarms() function defined in the Apex controller
		var action = component.get("c.getAccountId");
        action.setParams({
            "AccountId": component.get("v.recordId")
        });
         // Fetch the Alarm list from the Apex controller  
         helper.getAlarmList(component);
    }
    
})