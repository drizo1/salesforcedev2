({
    doInit: function(component, event, helper) {
		var callAction = component.get("c.getCall");
		callAction.setParams({"callId":component.get("v.recordId")});

		callAction.setCallback(this, function(response){
		var state = response.getState();
			if(state == "SUCCESS"){
				var call = response.getReturnValue();
				component.set("v.call", call);
				console.log('****************** call.KS_Telefono_Destino__c: ' + call.KS_Telefono_Destino__c);
				component.set("v.newContact.Phone", call.KS_Telefono_Destino__c);

			}else{
				console.log('Error obteniendo datos de la llamada.');
			}
		});
		$A.enqueueAction(callAction);

    },

    handleSave: function(component, event, helper) {
        //if(helper.validateContactForm(component)){
			console.log('v.selItem: ' + component.get("v.selItem.val"));
			var saveContactAction = component.get("c.saveContact");
			saveContactAction.setParams({
				"contact": component.get("v.newContact"),
				"accountId": component.get("v.selItem.val")
			});

			saveContactAction.setCallback(this, function(response){
				var state = response.getState();
				if(state == "SUCCESS"){
					var resultsToast = $A.get("e.force:showToast");
                    var staticLabel = $A.get("$Label.c.KS_Contact_saved");
                    var staticLabel2 = $A.get("$Label.c.KS_SavedC");
				
					resultsToast.setParams({
						"title": staticLabel,
						"message": staticLabel2
					});
					$A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
				}else if(state == "ERROR"){
					console.log('Problem saving contact, response state: ' + state);
				}else{
					console.log('Unknown problem, response state: ' + state);
				}
			});

			$A.enqueueAction(saveContactAction);

		//}
    }, 

	 handleCancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire(); 
    }

})