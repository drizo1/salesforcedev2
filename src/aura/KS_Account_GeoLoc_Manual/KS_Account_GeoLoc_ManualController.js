({
    doInit : function(component, event, helper) {
        
        $A.util.removeClass(component.find("header"), 'slds-theme_error');
        $A.util.removeClass(component.find("header"), 'slds-theme_alert-texture');
        component.set("v.error_msg", 'Location: Searching User Location.');
        
        function showError(cmp, isError, msgError) {
            var header = cmp.find("header");
            $A.util.removeClass(header, 'slds-theme_error');
            $A.util.removeClass(header, 'slds-theme_alert-texture');
            if(isError) {
                $A.util.addClass(header, 'slds-theme_error');
                $A.util.addClass(header, 'slds-theme_alert-texture');
            }
            cmp.set("v.error_msg", msgError);
        }
        
        var cuenta = component.get("v.recordId");
        if (cuenta == undefined || cuenta == null) { showError(component, true, 'Location: Account not found.'); return; }
        
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        if (!navigator.geolocation){ showError(component, true, 'Location: Geolocation is not supported by your browser'); return; }

        function success(position) {
            var evnt = component.getEvent("getLocation");
            evnt.setParams({
                "accID" : component.get("v.recordId"),
                "userLat" : position.coords.latitude,
                "userLon" : position.coords.longitude 
            });
            evnt.fire();
        }
        
        function error() { showError(component, true, 'Location: Unable to retrieve your location'); }
        navigator.geolocation.getCurrentPosition(success, error, options);
    },
    
    doManual : function(component, event, helper) {
        
        $A.util.removeClass(component.find("header"), 'slds-theme_error');
        $A.util.removeClass(component.find("header"), 'slds-theme_alert-texture');
        component.set("v.error_msg", 'Location: User Location Found!');
        
        function showError(cmp, isError, msgError) {
            var header = cmp.find("header");
            $A.util.removeClass(header, 'slds-theme_error');
            $A.util.removeClass(header, 'slds-theme_alert-texture');
            if(isError) {
                $A.util.addClass(header, 'slds-theme_error');
                $A.util.addClass(header, 'slds-theme_alert-texture');
            }
            cmp.set("v.error_msg", msgError);
        }
        
        var action = component.get("c.saveNewLocation");
        action.setParams({ 
            accID : event.getParam("accID"),//component.get("v.recordId"),
            userLat : event.getParam("userLat"),//position.coords.latitude,
            userLon : event.getParam("userLon")//position.coords.longitude 
        });
        action.setCallback(this, function(response) {
            var status = response.getReturnValue();
            if (status == 'ERROR_ACC') {
                showError(component, true, 'Location: Unable to process invalid Account.');
            } else if (status == 'ERROR_GEO_USER') {
                showError(component, true, 'Location: Unable to process invalid geolocation data.');
            } else if (status == 'INVALID') {
                showError(component, true, 'Location: Unable to Check In, Account is not at reach.');
            } else if (status == 'VALID') {
                showError(component, false, 'Location: Relocation Successful.');
                component.set('v.isLoading', false);
            } else {
                showError(component, true, 'Location: Unexpected Error.');
            }
        });
        $A.enqueueAction(action);
    }
})