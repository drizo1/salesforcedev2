({
    accountSelected : function(component, event, helper) {
        var event = $A.get("e.c:KS_Account_GeoLoc_Selected");
        event.setParams({"account": component.get("v.account")});
        event.fire();
    },
    accountGoToCall : function(component, event, helper) {
        var event = $A.get("e.c:KS_Account_GeoLoc_GoTo");
        event.setParams({"account": component.get("v.account")});
        event.fire();
	},
    accountGoTo: function(component, event, helper) {
        var account = event.getParam("account");
        helper.navigateToDetailsView(account.Id);
    }
})