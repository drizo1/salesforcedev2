({
    navigateToDetailsView : function(accountId) {
        var event = $A.get("e.force:navigateToSObject");
        if (event != undefined) {
            event.setParams({
                "recordId": accountId
            });
            event.fire();
        } else {
            location.href = '/'+accountId;
        }
    }
})