({

    handleSend: function(component, event, helper) {
        var action = component.get("c.sendMail");
        action.setParams({
            "destino": component.get("v.email"),
            "subject": component.get("v.asunto"),
            "body": component.get("v.body"),
            "parentID": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            $A.get("e.force:closeQuickAction").fire();
        });
        $A.enqueueAction(action);
    },
    
    handleCancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

})