({
    doSearch : function(component, event, helper) {

        var action = component.get("c.getOppsAlarm");
        action.setParams({
            recordId: event.getParam('recordId'),
            tipo: event.getParam('alarmType')
        });
        action.setCallback(this, function(data) {
            if (component.get("v.Alarmas")=='') {
                // Para que las listas no se pisen entre si
                component.set("v.Alarmas", data.getReturnValue());    
            }
        });
        $A.enqueueAction(action);
    },
    
    doMail : function(component, event, helper) {
        
        var attr = 'data-value' ;
        
        var res = null ; res = event.currentTarget.dataset.value;/*
        var oTarget = event.target ;
        if (oTarget.attributes[attr] != null)
            res = oTarget.attributes[attr].value;
        else
        {
            for (var i = 0; i < oTarget.attributes.length; i++)
            {
                if (oTarget.attributes[i].name === attr)
                    res = oTarget.attributes[i].value ;
            }
        }
        */
        if (res != null) {
            var event = $A.get("e.c:KS_Account_AlarmOpportunity_MailShow");
            event.setParams({"attachedURL": res, "isOpen": true});
            event.fire();
        }
    }
})