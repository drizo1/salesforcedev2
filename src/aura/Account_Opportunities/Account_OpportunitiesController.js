({
    doInit : function(component, event, helper) {
        var evnt = $A.get("e.c:KS_Account_AlarmOpportunity_doIt");
        evnt.setParams({"alarmType": "Opportunity", "recordId": component.get("v.recordId")});
        evnt.fire();
    }
})