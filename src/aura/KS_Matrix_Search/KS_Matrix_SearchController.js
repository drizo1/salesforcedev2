({
	doInit : function(component, event, helper) {
        var action = component.get("c.buildPickList");
        action.setCallback(this, function(a) {
            var returnValue = a.getReturnValue();
            
            component.set("v.matrixBSU", 'ALL');
            var bsuList = returnValue[0]; 
            var pickBSU = [bsuList.length];
            for (var i = 0; i < bsuList.length; i++) { 
                pickBSU[i] = { value:bsuList[i][0], label:bsuList[i][1] };
            }
            component.set("v.pickBSU", pickBSU);
            
            component.set("v.matrixFAM", 'ALL');
            var famList = returnValue[1];
            var pickFAM = [famList.length];
            for (var i = 0; i < famList.length; i++) { 
                pickFAM[i] = { value:famList[i][0], label:famList[i][1] };
            }
            component.set("v.pickFAM", pickFAM);
            
            var usrList = returnValue[2];
            var pickUSR = [usrList.length];
            for (var i = 0; i < usrList.length; i++) { 
                pickUSR[i] = { value:usrList[i][0], label:usrList[i][1] };
            }       
            component.set("v.pickUSR", pickUSR);
            component.set("v.matrixUSR", usrList[0][0]);
            
            var event = $A.get("e.c:KS_Matrix_Component_doIt");
            event.setParams({
                "matrixBSU": bsuList[0][0],
                "matrixFAM": famList[0][0],
                "matrixUSR": usrList[0][0]});
            event.fire();
        });
        $A.enqueueAction(action);
	},
    doSearch : function(component, event, helper) {
        var action = component.get("c.voidMethod");
        action.setCallback(this, function(a) {
            var event = $A.get("e.c:KS_Matrix_Component_doIt");
            event.setParams({
                "matrixBSU": component.find('picklistBSU').get('v.value'),
                "matrixFAM": component.find('picklistFAM').get('v.value'),
                "matrixUSR": component.find('picklistUSR').get('v.value')});
            event.fire();
        });
        $A.enqueueAction(action);
    }
})